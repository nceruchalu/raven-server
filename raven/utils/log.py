"""
Utility functions that are used when logging
"""

from django.utils import six
from django.core.files.uploadedfile import UploadedFile
from django.conf import settings

# Strings that if found in keys indicate that the key values should be masked
# when logging request/response data
SENSITIVE_KEYS = ['password']

# Strings that if used as keys indicate that the key values should be stripped
# when logging request/response data because they are too long
VERBOSE_KEYS = []

# Substitute for cleansed data
CLEANSED_SENSITIVE_SUBSTITUTE = "********************"
CLEANSED_VERBOSE_SUBSTITUTE = "<<<Verbose>>>"

def clean_request_data(request_data):
    """
    Clean request data to strip out sensitive keys and replace file objects
    with file names
    
    Args:
        request_data: Request data object
        
    Returns:
        cleaned request data object
    """    
    if isinstance(request_data, dict):
        cleansed_request_data = request_data.copy()
        
        for key, value in request_data.items():
            if isinstance(value, six.string_types):
                if any(k.lower() in key.lower() for k in SENSITIVE_KEYS):
                    cleansed_request_data[key] = CLEANSED_SENSITIVE_SUBSTITUTE
                    
                elif any(k.lower() == key.lower() for k in VERBOSE_KEYS):
                    cleansed_request_data[key] = CLEANSED_VERBOSE_SUBSTITUTE
                    
            elif isinstance(value, UploadedFile):
                cleansed_request_data[key] = value.name
                
    else:
        # If request data is not a dictionary then don't bother logging it.
        cleansed_request_data = "<<<Stripped>>>" if request_data else None
    
    return cleansed_request_data
    
