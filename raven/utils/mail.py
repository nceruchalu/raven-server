"""
Utility functions for sending mails
"""
from django.core.mail import send_mail
from django.template.loader import render_to_string

def send_mail_with_templates(context, from_email, recipient_list,
                             subject_template_name, message_template_name, 
                             html_message_template_name, request):
    """
    Send HTML email using templates
    
    Args:
        context: Email content context
        from_email: Sender email
        recipient_list: A list of recipient emails
        subject_template_name: Name of the subject template
        message_template_name: Name of the plain text email message template
        html_message_template_name: Name of the HTML email message template
        request: HttpRequest object
    """
    subject = render_to_string(subject_template_name, context, 
                                          request=request)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
        
    message = render_to_string(message_template_name, context, request=request)
    html_message = render_to_string(html_message_template_name, context,
                                    request=request)
    
    send_mail(subject=subject, message=message, from_email=from_email,
              recipient_list=recipient_list, html_message=html_message)
    
    
