"""
Utility classes to be used by the admin integrations by the apps
"""

from django.contrib import messages
from django.contrib.admin import helpers
from django.contrib.admin.utils import get_deleted_objects, model_ngettext
from django.core.exceptions import PermissionDenied, ValidationError
from django.core.urlresolvers import reverse
from django.db import router
from django.template.response import TemplateResponse
from django.utils.encoding import force_text
from django.utils.html import mark_safe
from django.utils.translation import ugettext as _, ugettext_lazy


class ModelAdminDeleteMixin(object):
    """
    Mixin class with custom admin actions for deleting selected objects.
    This adds the bonus of performing additional checks.
    """
    def delete_selected(self, request, queryset):
        """
        Admin action which deletes the selected objects by calling each object's
        delete() method, as opposed to using the default `delete_selected()` 
        admin action that only calls the QuerySet's delete() method. Note that
        a QuerySet delete() method doesn't call individual object delete() 
        methods, and this means it could skip some important cleanup.
        
        This action first displays a confirmation page whichs shows all the
        deleteable objects, or, if the user has no permission one of the related
        childs (foreignkeys), a "permission denied" message.
        
        Next, it deletes all selected objects and redirects back to the change 
        list.
    
        Args:
            self: the current ModelAdmin
            request: an HttpRequest representing the current request
            queryset: a QuerySet containing the set of selected objects
                        
        Returns:
            A TemplateResponse to display an intermediary confirmation page, or
            None if the user is simply redirected back to the original change 
            list page. 
                        
        Raises:
            PermissionDenied: User does not have delete permission for the 
                actual model or related models (related via foreign keys).
        """
        modeladmin = self
        opts = modeladmin.model._meta
        app_label = opts.app_label

        # Check that the user has delete permission for the actual model
        if not modeladmin.has_delete_permission(request):
            raise PermissionDenied
        
        # perform custom validation
        try:
            self.validate_selected(request, queryset)
        except ValidationError as e:
            modeladmin.message_user(request, e.message, messages.ERROR)

        using = router.db_for_write(modeladmin.model)

        # Populate deletable_objects, a data structure of all related objects 
        # that will also be deleted.
        deletable_objects, model_count, perms_needed, protected = (
            get_deleted_objects(
                queryset, opts, request.user, modeladmin.admin_site, using))

        # The user has already confirmed the deletion.
        # Do the deletion and return a None to display the change list view 
        # again.
        if request.POST.get('post') and not protected:
            if perms_needed:
                raise PermissionDenied
            n = queryset.count()
            if n:
                for obj in queryset:
                    obj_display = force_text(obj)
                    modeladmin.log_deletion(request, obj, obj_display)
                    obj.delete()
                modeladmin.message_user(
                    request, 
                    _("Successfully deleted %(count)d %(items)s.") % {
                        "count": n, "items": model_ngettext(modeladmin.opts, n)
                        }, 
                    messages.SUCCESS)
            # Return None to display the change list page again.
            return None

        if len(queryset) == 1:
            objects_name = force_text(opts.verbose_name)
        else:
            objects_name = force_text(opts.verbose_name_plural)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": objects_name}
        else:
            title = _("Are you really sure?")

        context = dict(
            modeladmin.admin_site.each_context(request),
            title=title,
            objects_name=objects_name,
            deletable_objects=[deletable_objects],
            model_count=dict(model_count).items(),
            queryset=queryset,
            perms_lacking=perms_needed,
            protected=protected,
            opts=opts,
            action_checkbox_name=helpers.ACTION_CHECKBOX_NAME,
            media=modeladmin.media,
            )

        request.current_app = modeladmin.admin_site.name

        # Display the confirmation page
        return TemplateResponse(
            request, 
            modeladmin.delete_selected_confirmation_template or [
                "admin/%s/%s/delete_selected_confirmation.html" % (
                    app_label, opts.model_name),
                "admin/%s/delete_selected_confirmation.html" % app_label,
                "admin/delete_selected_confirmation.html"
                ], 
            context)

    delete_selected.short_description = ugettext_lazy(
        "Delete selected %(verbose_name_plural)s")
    
    def validate_selected(self, request, queryset):
        """
        Extra validation to be run on the selected objects before deletion.
        Subclass this method and raise a validation error with a message if
        something goes wrong.
        
        Args:
            self: the current ModelAdmin
            request: an HttpRequest representing the current request
            queryset: a QuerySet containing the set of selected objects
                        
        Returns:
            None
            
        Raises:
            ValidationError: selected objects cannot be deleted for reason
                stated in exception's message.
        """
        pass
        

class ChangeFormReadOnlyMixin(object):
    """
    Mixin class that has some fields be read-only in both object change forms
    and add forms, and other fields be read-only in only change forms
    
    Using this mixin requires overriding either or both of readonly_fields
    and change_readonly_fields
    as specified by Django's ModelAdmin, and the 
    """
    # read-only fields only available on the change form
    change_readonly_fields = ()
    
    def _get_readonly_fields(self, request, obj=None):
        """
        Generate a collection of field names that will be displayed as read-only
        
        Args:
            request: HttpRequest object
            obj: Object being edited (or None on an add form)
        """
        readonly_fields = list(self.readonly_fields)
        if obj is not None:
            readonly_fields.extend(list(self.change_readonly_fields))
        return readonly_fields


def add_link_field(target_model=None, field='', app='', field_name='link',
                   link_text=unicode, short_description=""):
    """
    Add a link from a change list or change form of a source object to the 
    change form of a related (target) object.
    
    This makes it possible to have multiple links from one ModelAdmin object
    and to link to a model in a different app
    
    Args:
        target_model: name of the model to link to
        field: field name on the target/related object on the source object
        app: app that the target model exists in. If not specified it
            defaults to same app as the source model.
        field_name: Name of this link field as would be used on the ModelAdmin
            object
        link_text: function that takes an argument of the related object and 
            returns a string representation of it
        short_description: verbose name of link field. If it isn't specified it
            defaults to "{target_model} link"
        
    Usage:
        # 'apple' is the name of model to link to 
        # 'fruit_food' is field name in `instance` so 
        #     instance.fruit_food = Apple()
        # 'link2' will be name of this field on the model admin instance
        @add_link_field(target_model='apple', field='fruit_food', 
            field_name='link2')
        # 'cheese' is name of model to link to
        # 'milk_food' is field name in `instance` so 
        #     instance.milk_food = Cheese()
        # 'milk' is the name of the app where Cheese lives
        @add_link_field(target_model='cheese', field='milk_food', 
            field_name='link')
        class FoodAdmin(admin.ModelAdmin):
            list_display = ('id', 'link', 'link2')
      
    Ref:
        http://stackoverflow.com/a/13287201        
    """
    def add_link(cls):
        """
        Add link to the ModelAdmin class
        
        Args:
            cls: ModelAdmin class
        """
        reverse_name = target_model.lower() or cls.model.__name__.lower()
        def link(self, instance):
            """
            Create the link to the related object of the ModelAdmin object
            instance.
            
            Args:
                instance: Source object currently represented by ModelAdmin 
                    object
                    
            Returns:
                HTML string of anchor tag element
            """
            app_name = app or instance._meta.app_label
            reverse_path = "admin:{app_name}_{reverse_name}_change".format(
                app_name=app_name, reverse_name=reverse_name)
            link_obj = getattr(instance, field, None)
            if link_obj is not None:
                url = reverse(reverse_path, args = (link_obj.id,))
            else:
                url = "javascript:;"
            return mark_safe('<a href="{url}">{url_text}</a>'.format(
                    url=url, url_text=link_text(link_obj).encode('utf-8')))
        link.allow_tags = True
        link.short_description = (short_description or 
                                  (reverse_name + ' link'))
        setattr(cls, field_name, link)
        cls.readonly_fields = list(getattr(cls, 'readonly_fields', [])) + [
            field_name]
        return cls
    return add_link

