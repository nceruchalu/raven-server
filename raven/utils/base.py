"""
Utility functions that come in handy through the project

Table Of Contents:
    slugify:             slugify any given string
    get_upload_path:     determine a unique upload path for a given file
    list_dedup:          dedup a list and preserve order of elements
    human_readable_size: human readable size from byte count
"""

import os, re, unicodedata, uuid


def slugify(string):
    """
    Slugify any given string with following rules:
    - ASCII encoding
    - Non-alphanumerics are replaced with hyphens
    - groups of hyphens will be replaced with a single hyphen
    - slugs cannot begin/end with hyphens.
    
    Args:   
        string: string to get slugged version of
    Returns:      
        ASCII slugified version of input string
    """
    s = unicode(string)
    # Get normal form 'NFKD' for the unicode version of passed string
    slug = unicodedata.normalize('NFKD', s)
    # Set result to use ASCII encoding
    slug = slug.encode('ascii', 'ignore').lower()
    # Replace all non-alphanumerics with hyphens '-', and strip() any hyphens
    slug = re.sub(r'[^a-z0-9]+', '-', slug).strip('-')
    # finally, replace groups of hyphens with a single hyphen
    slug = re.sub(r'[-]+', '-', slug)
    return slug


def get_upload_path(instance, filename, root, basename=''):
    """
    Determine a unique upload path for a given file
    
    Args:
        instance: model instance where the file is being attached
        filename: filename that was originally given to the file
        root: root folder to be prepended to file upload path. Example values 
            are 'photo/', 'photo' [note the ending slash doesn't matter] 
        basename: Base name to use in place of appending a unique string to
            the filename
         
    Returns:      
        Unique filepath for given file, that's a subpath of `root`
    """
    name = os.path.basename(filename).split('.')
    if not basename:
        unique_hash = unicode(uuid.uuid4()).replace('-','')[:15]
        basename = slugify(name[0][:8]) + '_' + unique_hash
    
    base_path = basename + "." + name[len(name)-1]
    return os.path.join(root, base_path)


def list_dedup(in_list):
    """    
    Dedup a list and preserve order
          
    Args:   
        in_list: list to have its duplicates removed
        
    Returns:      
        Dedup'd version of passed list
    """
    seen = set()
    return [s for s in in_list if s not in seen and not seen.add(s)]


def human_readable_size(num):
    """
    Present a human readable size string from bytes count.
        >>> human_readable_size(2048)
        '2.0 KB'
    Ref: http://stackoverflow.com/a/1094933
    
    Args:
        num: number of bytes to be converted
    
    Returns:
        A string that represents a human readable size
    """
    for x in ['bytes', 'KB', 'MB', 'GB']:
        if num < 1024.0 and num > -1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0
    return "%3.1f %s" % (num, 'TB')
    

def human_readable_count(num):
    """
    Present a human readable count string from a number count.
        >>> human_readable_count(2048)
        '2.0 K'
    Ref: http://stackoverflow.com/a/1094933
    
    Args:
        num: number to be converted
    
    Returns:
        A string that represents a human readable count
    """
    for x in ['', 'K', 'M', 'B']:
        if num < 1000.0 and num > -1000.0:
            return num if x == '' else "%3.1f %s" % (num, x) 
        num /= 1000
    return "%3.1f %s" % (num, 'T')
    
