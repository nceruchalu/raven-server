"""
Customizations to rest framework.pagination
"""
from collections import OrderedDict

from rest_framework import pagination
from rest_framework.response import Response
from rest_framework.settings import api_settings

class PageNumberPagination(pagination.PageNumberPagination):
    """
    A simple page number based style that supports page numbers as
    query parameters. For example:
    
    http://api.example.org/accounts/?page=4
    http://api.example.org/accounts/?page=4&page_size=100
    """
    # Client can control the page size using this query parameter.
    page_size_query_param = 'page_size'
    
    # Set to an integer to limit the maximum page size the client may request.
    # Only relevant if 'page_size_query_param' has also been set.
    max_page_size = api_settings.PAGE_SIZE
    
    last_page_strings = ('last', '-1', -1,)
    
    def get_paginated_response(self, data):        
        """
        Modify original implementation to add:
            final: the total number of pages
            current: the current page number
        """
        return Response(OrderedDict([
                    ('count', self.page.paginator.count),
                    ('current', self.page.number),
                    ('next', self.get_next_link()),
                    ('previous', self.get_previous_link()),
                    ('final', self.page.paginator.num_pages),
                    ('results', data)
                    ]))
