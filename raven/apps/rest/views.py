"""
Customizations to rest_framework.views
"""
import json
import logging

from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils.encoding import smart_text

import markdown

from rest_framework.authentication import get_authorization_header
from rest_framework.exceptions import APIException
from rest_framework.renderers import JSONRenderer
from rest_framework.utils import formatting
from rest_framework.views import exception_handler as rest_exception_handler

from raven.utils.log import clean_request_data


def apply_markdown(text):
    """
    Simple wrapper around :func:`markdown.markdown` to set the base level
    of '#' style headers to <h2>.
    """
    extensions = ['headerid(level=2)', 'tables', 'fenced_code']
    safe_mode = False
    md = markdown.Markdown(extensions=extensions, safe_mode=safe_mode)
    return md.convert(text)


def markup_description(description):
    """
    Apply HTML markup to the given description.
    """
    description = apply_markdown(description)
    return mark_safe(description)


def get_view_description(view_cls, html=False):
    """
    Given a view class, return a textual description to represent the view.
    This name is used in the browsable API, and in OPTIONS responses.

    This function is the default for the `VIEW_DESCRIPTION_FUNCTION` setting.
    """
    description = view_cls.__doc__ or ''
    description = formatting.dedent(smart_text(description))
    if html:
        return markup_description(description)
    return description


def exception_handler(exc, context):
    """
    Returns the response that should be used for any given exception.
    
    By default we handle the REST framework `APIException`, and also
    Django's built-in `Http404` and `PermissionDenied` exceptions.
    
    Any unhandled exceptions may return `None`, which will cause a 500 error
    to be raised.
    """
    # The default exception handler will still be used as is. However 
    # it will have its contents logged
    response = rest_exception_handler(exc, context)
    
    if settings.DEBUG:
        # extract request and response from the request
        request = context['request']
        try:
            request_data = clean_request_data(request.data)
        except APIException:
            request_data = None
            
        authorization_header =get_authorization_header(request._request) or None
        request_context = {'method': request.method,
                           'path': request._request.path,
                           'data': request_data,
                           'query_params': request.query_params,
                           'authorization_header': authorization_header}
        
        response_context = None
        if response:
            response_data = (JSONRenderer().render(response.data) 
                             if response.data else None)
            response_context = {'data': response_data,
                                'status_code': response.status_code}
            
            
        logger = logging.getLogger(settings.LOGGER_NAME_LOGGLY)
        logger.info(json.dumps({
                    'source': 'rest.exception_handler',
                    'request': request_context,
                    'response': response_context
                    }))
        
    return response
