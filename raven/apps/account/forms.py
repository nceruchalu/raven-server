"""
When using a custom user model some built-in auth forms must be rewritten,
UserCreationForm and UserChangeForm
"""

from django import forms
from django.contrib.auth import password_validation
from django.utils.translation import ugettext_lazy as _
import django.contrib.auth.forms as auth_forms

from raven.apps.account.models import User, RegistrationProfile


class UserCreationForm(auth_forms.UserCreationForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    
    This overrides the Django form to use the custom User model and replace the
    username field with an email field.
    """
    class Meta:
        model = User
        fields = ("email",)


class UserChangeForm(auth_forms.UserChangeForm):
    """
    A form for updating users. Includes all the fields on the user, but replaces
    the password field with admin's password hash display field.
    
    This overrides the Django form to use the custom User model.
    """
    class Meta:
        model = User
        fields = '__all__'


class RegistrationForm(forms.Form):
    """
    Serializer to be used for creating a user
    """
    email = forms.EmailField(label='Email Address')
    
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput)
    
    def clean_email(self):
        """
        Ensure the email address doesn't already exist
        """
        data = self.cleaned_data['email'].lower()
        if User.objects.filter(email__iexact=data).exists():
            raise forms.ValidationError(
                "An account with this email already exists.")
        
        # Always return the cleaned data, whether you have changed it or not.
        return data
     
    def clean_password(self):
        """
        Check that the password complies with the password validation settings
        """
        data = self.cleaned_data['password']
        password_validation.validate_password(data)
        
        # Always return the cleaned data, whether you have changed it or not.
        return data
    
    def register(self, request):
        """
        Register the user and create a team after validating and cleaning the
        form data
        
        Args:
            request: HttpRequest object
            
        Returns:
            Created User object
        """
        # create an inactive user and send an activation email
        user = RegistrationProfile.objects.get_or_create_user(
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password'],
            is_active=False,
            request=request)
        
        return user
