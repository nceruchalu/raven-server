from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

# Using modified versions of django registration password reset templates.
# Find originals here: https://github.com/django/django/tree/master/django/contrib/admin/templates/registration

urlpatterns = [
    # --------------------------------------------------------------------------
    # Email verification and account activation
    # --------------------------------------------------------------------------
    # Activate a user's profile or show error message
    # Activation keys get matched by \w+ instead of the more specific 
    # [a-fA-F0-9]{40} because a bad key should still get to the view; 
    # that way it can return a sensible "invalid key" message instead of a 
    # confusing 404.
    url(r'^activate/(?P<activation_key>\w+)/$',
        TemplateView.as_view(template_name='handled_by_client.html'),
        name='acct_activate'),
        
    # --------------------------------------------------------------------------
    # Password Reset
    # --------------------------------------------------------------------------
    # Reset password by providing a new password
    url(r'^password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        TemplateView.as_view(template_name='handled_by_client.html'),
        name='acct_password_reset_confirm'),
    ]
