from collections import OrderedDict
from datetime import timedelta

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.utils import timezone
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View

from rest_framework import (
    generics, status, permissions, parsers, serializers, renderers, exceptions)
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

from raven.apps.account.models import User, AuthToken, RegistrationProfile
from raven.apps.account.serializers import (
    UserPublicSerializer, UserPrivateSerializer, UserCreationSerializer,
    AuthTokenSerializer, AuthTokenVerificationSerializer,
    ActivationKeySerializer,
    PasswordResetCheckSerializer, PasswordResetConfirmSerializer,
    PasswordResetSerializer, PasswordChangeSerializer)
from raven.apps.account.permissions import (
    IsOwnerOrReadOnly, IsDetailOwner)

from raven.utils.mail import send_mail_with_templates

# Create your views here.

# -----------------------------------------------------------------------------
# USERS LIST
# -----------------------------------------------------------------------------

class UserList(generics.CreateAPIView):
    """
    Create a new user.
        
    ## Reading
    You can't read using this endpoint. While it would seem nice for this API 
    endpoint to list all users you can imagine why this is a user privacy issue.
    
    
    ## Publishing
    ### Permissions
    * Anyone can create using this endpoint.
    
    ### Fields
    Parameter     | Description                                  | Type
    ------------- | -------------------------------------------- | ---------- 
    `email`       | email address for the new user. **Required** | _string_
    `password`    | password for the new user. **Required**      | _string_
    
    ### Response
    If create is successful, a limited scope user object, otherwise an error 
    message. 
    The limited scope user object has the following fields:
    
    Name          | Description                               | Type
    ------------- | ----------------------------------------- | ---------- 
    `url`         | URL of new user object                    | _string_
    `id`          | The ID of the newly created user          | _integer_
    `email`       | email address as provided during creation | _string_
        
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    
    
    ## Endpoints
    Name                                     | Description                      
    ---------------------------------------- | ---------------------------------
    [`/users/<email>/`](hello@tryraven.com/) | Get/Update a user
    
    ##
    """
    authentication_classes = () # No need to authenticate, wastes time
    permission_classes = (permissions.AllowAny,)
    queryset = User.objects.all()
            
    def get_serializer_class(self):
        """
        A POST request implies a user creation so return the serializer for
        user creation. 
        All other possible requests will be GET so use the privacy-respecting
        user serializer
        """
        if self.request.method == "POST":
            return UserCreationSerializer
        else: 
            return UserPublicSerializer


# -----------------------------------------------------------------------------
# USER'S DETAILS
# -----------------------------------------------------------------------------

class UserDetail(generics.RetrieveUpdateAPIView):
    """
    Retrieve or update a user instance
    
    ## Reading
    ### Permissions
    * Only authenticated users can read this endpoint.
    * Authenticated users reading their own user instance get additional private
      data.
    
    ### Fields
    Reading this endpoint returns a user object containing the public user data.
    An authenticated user reading their own user also gets to see the private
    user data.
    
    Name               | Description                            | Type
    ------------------ | -------------------------------------- | ---------- 
    `url`              | URL of user object                     | _string_
    `id`               | ID of user                             | _integer_
    `email`            | email of user object                   | _string_
    `first_name`       | first name of user object              | _string_
    `last_name`        | last name of user object               | _string_
    `is_active`        | Is the user's account activated?       | _boolean_
    `updated_at`       | last modified date/time of user object | _date/time_
    `created_at`       | creation date/time of user object      | _date/time_
    
    
    ## Publishing
    You can't create using this endpoint
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    ### Permissions
    * Only authenticated users can write to this endpoint.
    * Authenticated users can update their own user instance
    
    ### Fields
    Parameter    | Description                                | Type
    ------------ | ------------------------------------------ | ----------
    `first_name` | new first name for the user                | _string_
    `last_name`  | new last name for the user                 | _string_
            
    ### Response
    If update is successful, a user object, otherwise an error message.
    """
    permission_classes = (permissions.IsAuthenticated,
                          IsOwnerOrReadOnly,)
    queryset = User.objects.all()
    
    # lookup by 'email' not the 'pk' but allow for case-insensitive lookups
    lookup_field = 'email__iexact'
    lookup_url_kwarg = 'email'
        
    def get_serializer_class(self):
        """
        You only get to see private data if you request details of yourself.
        Otherwise you get to see a limited view of another user's details
        """
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        lookup = self.kwargs[lookup_url_kwarg]
        
        if (self.request.user.email.lower() == lookup.lower()):
            serializer_class = UserPrivateSerializer
        else:
            serializer_class = UserPublicSerializer
        
        return serializer_class
    

# -----------------------------------------------------------------------------
# AUTHENTICATED USER'S DETAILS
# -----------------------------------------------------------------------------
class AuthenticatedUserDetail(generics.RetrieveUpdateAPIView):
    """
    Retrieve or update the authenticated user
    
    ## Reading
    ### Permissions
    * Authenticated users only
    
    ### Fields
    Reading this endpoint returns a user object containing the authenticated
    user's public and private data.
    
    Name               | Description                            | Type
    ------------------ | -------------------------------------- | ---------- 
    `url`              | URL of user object                     | _string_
    `id`               | ID of user                             | _integer_
    `email`            | email of user object                   | _string_
    `first_name`       | first name of user object              | _string_
    `last_name`        | last name of user object               | _string_
    `is_active`        | Is the user's account activated?       | _boolean_
    `updated_at`       | last modified date of user object      | _date/time_
    `created_at`       | creation date/time of user object      | _date/time_
    
    
    ## Publishing
    You can't create using this endpoint
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    ### Permissions
    * Only authenticated users can write to this endpoint.
    * Authenticated users can only update their own user instance.
    
    ### Fields
    Parameter    | Description                                | Type
    ------------ | ------------------------------------------ | ----------
    `first_name` | new first name for the user                | _string_
    `last_name`  | new last name for the user                 | _string_
    
    ### Response
    If update is successful, a user object containing public and private data, 
    otherwise an error message.
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UserPrivateSerializer
    queryset = User.objects.all()
    
    def get_object(self):
        """
        Simply return authenticated user.
        No need to check object level permissions
        """
        return self.request.user


# ------------------------------------------------------------------------------
# USER ACCOUNT AND CREDENTIALS MANAGEMENT
# ------------------------------------------------------------------------------
@api_view(('GET',))
def account_root(request, format=None):
    """
    List the endpoints used for account and credentials managements.
    """
    return Response(
            OrderedDict([
                ('Obtain Authentication Token', reverse(
                'obtain_auth_token', request=request, format=format)),
            
                ('Verify authentication token', reverse(
                'verify_auth_token', request=request, format=format)),
            
                ('Activate Account', reverse(
                        'acct_activate_api', request=request, format=format)),
                
                ('Password management', reverse(
                        'password_root', request=request, format=format)),
                ])
            )


@api_view(('GET',))
def password_root(request, format=None):
    """
    List of endpoints used for password management.
    """
    return Response(
        OrderedDict([
                ('Password reset (begin)', reverse(
                        'acct_password_reset', request=request, format=format)),
            
                ('Password reset (check)', reverse(
                'acct_password_reset_check', request=request, format=format)),
            
                ('Password reset (complete)', reverse(
                        'acct_password_reset_confirm_api', request=request,
                        format=format)),
            
                ('Password change', reverse(
                        'acct_password_change', request=request,
                        format=format)),
                ])
        )


class ObtainExpiringAuthToken(ObtainAuthToken):
    """
    Subclass of `rest_framework.authtoken.views.ObtainAuthToken` that refreshes 
    and returns the authentication token each time it is requested.
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    ### Permissions
    * Anyone can __POST__ using this endpoint.
    
    ### Fields
    Parameter      | Description                                 | Type
    -------------- | ------------------------------------------- | ---------- 
    `email`        | email for the user. **Required**            | _string_
    `password`     | password for the user. **Required**         | _string_
    
    ### Response
    If  is successful, the authentication token, otherwise an error message. 
    
    Receiving the HTTP CODE, `HTTP_400_BAD_REQUEST`, means that maybe you
    should try re-activating this user account or creating a new one.
    
    Name           | Description                              | Type
    -------------- | ---------------------------------------- | ---------- 
    `token`        | Authentication Token for request headers | _string_
    `expires_at`   | Expiration date of the token             | _date/time_
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    
    
    ## Endpoints
    Name                   | Description          
    ---------------------- | ---------------------------------------------------
    [`verify/`](verify/)   | Verify the received authentication token
    
    ##
    """
    serializer_class = AuthTokenSerializer
    authentication_classes = () # No need to authenticate, as it wastes time
    permission_classes = (permissions.AllowAny,)
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.BrowsableAPIRenderer,)
        
    def post(self, request,  *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        # Validate serializer fields and get authenticated user
        try:
            user = self.authenticate_user(request, serializer)       
        except serializers.ValidationError, e:
            return Response(
                {api_settings.NON_FIELD_ERRORS_KEY: e.detail}, 
                status=status.HTTP_400_BAD_REQUEST)

        
        # get or create user's corresponding token
        token, created = AuthToken.objects.get_or_create(user=user)
        
        # a stale token is one that is older than SESSION_COOKIE_AGE
        stale_token = ((timezone.now() - token.updated_at) > 
                       timedelta(seconds=settings.SESSION_COOKIE_AGE))
            
        if not created and stale_token:
            # refresh the token if it isn't newly created and it's stale by
            # updating the token key and saving. The updated_at field will
            # automatically be set to now()
                            
            # attempt creating a new and unique key. Don't want to keep looping
            # while making DB calls so will give this one-shot. If the
            # first key generated is unique then use it, else tough luck.
            new_key = token.generate_key()
            if AuthToken.objects.filter(key=new_key).count() == 0:
                token.key = new_key
            
            # saving auto-updates the updated_at field.
            token.save()
            
        expires_at = token.updated_at + timedelta(
            seconds=settings.SESSION_COOKIE_AGE)
        datetime_format = settings.REST_FRAMEWORK['DATETIME_FORMAT']
            
        # finally return token in response
        return Response({'token': token.key,
                         'expires_at': expires_at.strftime(datetime_format)})

    def authenticate_user(self, request, serializer):
        """
        Authenticate user with serializer fields
        
        Args:
            request: rest_framework.request.Request object
            serializer: validated serializer object instance
            
        Raises:
            serializers.ValidationError: Authentication Error
            
        Returns:
            Authenticated user object
        """
        email = serializer.validated_data['email']
        password = serializer.validated_data['password']
        user = authenticate(email=email, password=password)

        if not user or not user.is_active:
            # User not be authenticated or user is authenticated and inactive
            if user:
                # User credentials are valid but user is inactive
                msg = _('User account is disabled.')
                invalid_user = user
                
            else:
                # User couldn't be logged in with given credentials, but let's
                # still check for a user that matches the given email
                # and ensure it's actually activated
                msg = _('Unable to log in with provided credentials.')
                try:
                    invalid_user = User._default_manager.select_related(
                        'registration_profile').get(email__iexact=email)
                
                except User.DoesNotExist:
                    invalid_user = None
            
            if invalid_user and not invalid_user.is_active:
                # Check if user is inactive because they are yet to confirm 
                # email by checking the registration and invitation profiles
                # First attempt checking a registration profile
                try:
                    registration_profile = invalid_user.registration_profile
                    if (not registration_profile.is_activated and
                        not registration_profile.activation_key_expired()):
                        msg = _("You are yet to confirm your account used "
                                "during registration so we just resent your "
                                "confirmation email.")
                        registration_profile.send_activation_email(request)
                        
                except RegistrationProfile.DoesNotExist:
                    registration_profile = None
                                    
            raise serializers.ValidationError(msg)
            
        return user


class VerifyExpiringAuthToken(generics.GenericAPIView):
    """
    Verify the authentication token matches the given user.
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    ### Permissions
    * Anyone can __POST__ using this endpoint.
    
    ### Fields
    Parameter    | Description                                   | Type
    ------------ | --------------------------------------------- | ---------- 
    `token`      | Authentication token to verify. **Required**  | _string_
    `email`      | email for the user. **Required**              | _string_
    
    ### Response
    If is successful, the authentication token and expiration date, otherwise an
    error message.
    
    Receiving the HTTP CODE, `HTTP_400_BAD_REQUEST`, means that maybe you
    should try re-obtaining the authentication token.
    
    Name           | Description                              | Type
    -------------- | ---------------------------------------- | ---------- 
    `token`        | Authentication Token for request headers | _string_
    `expires_at`   | Expiration date of the token             | _date/time_
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    """
    serializer_class = AuthTokenVerificationSerializer
    authentication_classes = () # No need to authenticate, as it wastes time
    permission_classes = (permissions.AllowAny,)
            
    def post(self, request,  *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        # Get auth token object matching token key
        key = serializer.validated_data['token']
        token = generics.get_object_or_404(AuthToken, key=key)
        
        # Get user matching the email
        email = serializer.validated_data['email']
        user = generics.get_object_or_404(User, email__iexact=email)
        
        if token.user_id != user.id:
             return Response(
                {api_settings.NON_FIELD_ERRORS_KEY: [
                        "Credentials don't match authentication token."]}, 
                status=status.HTTP_400_BAD_REQUEST)
        
        # a stale token is one that is older than SESSION_COOKIE_AGE
        stale_token = ((timezone.now() - token.updated_at) > 
                       timedelta(seconds=settings.SESSION_COOKIE_AGE))
        if stale_token:
            return Response(
                {api_settings.NON_FIELD_ERRORS_KEY: [
                        "Authentication token is stale."]}, 
                status=status.HTTP_400_BAD_REQUEST)
                
        expires_at = token.updated_at + timedelta(
            seconds=settings.SESSION_COOKIE_AGE)
        datetime_format = settings.REST_FRAMEWORK['DATETIME_FORMAT']
            
        # finally return token in response
        return Response({'token': token.key,
                         'expires_at': expires_at.strftime(datetime_format)})


class Activate(generics.GenericAPIView):
    """
    Given an activation key generated during user signup, look up and activate
    the user account corresponding to that key (if possible).
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    ### Permissions
    * Anyone can __POST__ using this endpoint.
    
    ### Fields
    Parameter        | Description                                 | Type
    ---------------- | ------------------------------------------- | ----------
    `activation_key` | User's activation key. **Required**         | _string_
    
    ### Response
    If is successful, the authentication details, otherwise an error message. 
    The reason for returning authentication details is it makes it easy to
    auto-login the user after successful activation.
    
    Name           | Description                              | Type
    -------------- | ---------------------------------------- | ---------- 
    `email`        | Activated user's email                   | _string_
    `token`        | Authentication Token for request headers | _string_
    `expires_at`   | Expiration date of the token             | _date/time_
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    """
    authentication_classes = () # No need to authenticate, as it wastes time
    permission_classes = (permissions.AllowAny,)
    serializer_class = ActivationKeySerializer
        
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
                
        activation_key = serializer.validated_data['activation_key']
        activated_user =  RegistrationProfile.objects.activate_user(
            activation_key)
        
        if activated_user is not None:
            # successfully activated user so generate authentication data
            return Response(
                self.generate_auth_details(activated_user), 
                status=status.HTTP_200_OK)
        
        else:
            # activation failed, so use the registration_profile (if one
            # exists) to explain what went wrong.
            error_msg = "Unable to activate account."
            try:
                registration_profile = RegistrationProfile.objects.get(
                    activation_key=activation_key)
                if registration_profile.is_activated:
                    error_msg = "Activation link has already been used."
                elif registration_profile.activation_key_expired():
                    error_msg = ("Activation link has expired. "
                                 "Please request another.")

            except RegistrationProfile.DoesNotExist:
                # activation key doesn't exist
                error_msg = "Activation link is invalid."
                
            return Response(
                {api_settings.NON_FIELD_ERRORS_KEY: [error_msg]}, 
                status=status.HTTP_400_BAD_REQUEST)
        
    def generate_auth_details(self, user):
        """
        Generate the authentication credentials response to be returned
        upon successul activation
        
        Args:
            user: successfully activated user
            
        Returns:
            Dictionary containing user's email, authentication token, and
            token's expiry date
        """
        # generate an authentication token
        token, created = AuthToken.objects.get_or_create(user=user)
        token.key = token.generate_key()
        token.save()
            
        expires_at = token.updated_at + timedelta(
            seconds=settings.SESSION_COOKIE_AGE)
        datetime_format = settings.REST_FRAMEWORK['DATETIME_FORMAT']
            
        # finally authentication details
        return {'email': user.email,
                'token': token.key,
                'expires_at': expires_at.strftime(datetime_format)}


# 3 views for password reset:
# - PasswordReset sends the mail with the reset link contain a uid and token
# - PasswordResetCheck checks a given uid and token 
# - PasswordResetConfirm checks a given uid and token and if valid sets
#   a new password
class PasswordReset(generics.GenericAPIView):
    """
    Reset password and send password reset email
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    ### Permissions
    * Anyone can create using this endpoint.
    
    ### Fields
    Parameter    | Description                              | Type
    ------------ | ---------------------------------------- | ---------- 
    `email`      | email for the new user. **Required**     | _string_
    
    ### Response
    If create is successful, an **HTTP 204**, otherwise an error message. 
    The limited scope user object has the following fields:
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    
    
    ## Endpoints
    Name                   | Description          
    ---------------------- | ---------------------------------------------------
    [`check/`](check/)     | Validate the password reset link components
    [`confirm/`](confirm/) | Validate reset link and set new password
    
    ##
    """
    authentication_classes = () # No need to authenticate, as it wastes time
    permission_classes = (permissions.AllowAny,)
    serializer_class = PasswordResetSerializer
    queryset = User.objects.all()
    
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, )
        serializer.is_valid(raise_exception=True)
        
        email = serializer.validated_data['email']
        
        try:
            user = User.objects.get(email__iexact=email)
            self.send_email(request, user)
            return Response(status=status.HTTP_204_NO_CONTENT)
        
        except User.DoesNotExist:
            return Response(
                {api_settings.NON_FIELD_ERRORS_KEY: ["User does not exist"]}, 
                status=status.HTTP_400_BAD_REQUEST)
    
    def send_email(self, request, user):
        """
        Generate a one-use only link for resetting password and send to the
        user
        
        Args:
            request:  rest_framework.Request object to be used in 
                determining the password reset email link
            user: User requesting the password reset email
                
        Returns:
            None
        """
        subject_template_name = 'account/password_reset_email_subject.txt'
        message_template_name = 'account/password_reset_email.txt'
        html_message_template_name ='account/password_reset_email.html'
                
        uid = urlsafe_base64_encode(force_bytes(user.pk))
        token =  default_token_generator.make_token(user)
        
        reset_link = reverse(
            'acct_password_reset_confirm',
            request=request,
            kwargs={'uidb64': uid, 'token': token})
        
        context = {
            'title': "Reset your password",
            'reset_link': reset_link}
        
        send_mail_with_templates(
            context=context,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[user.email],
            subject_template_name=subject_template_name,
            message_template_name=message_template_name,
            html_message_template_name=html_message_template_name,
            request=request._request)
        

class PasswordResetCheck(generics.GenericAPIView):
    """
    Check the components of the password reset link, the base64 encoded user id
    and the user-specific token
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    ### Permissions
    * Anyone can **POST** using this endpoint.
    
    ### Fields
    Parameter      | Description                                  | Type
    -------------- | -------------------------------------------- | ---------
    `uidb64`       | Base 64 encoded user id. **Required**        | _string_
    `token`        | Reset token specific to user. **Required**   | _string_
    
    ### Response
    If request is successful, a limited scope user object, otherwise an error
    message. 
        
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    """
    authentication_classes = () # No need to authenticate, as it wastes time
    permission_classes = (permissions.AllowAny,)
    serializer_class = PasswordResetCheckSerializer
    queryset = User.objects.all()
    
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, )
        serializer.is_valid(raise_exception=True)
        
        uidb64 = serializer.validated_data['uidb64']
        token = serializer.validated_data['token']
        
        validlink, user = self.check_reset_link(uidb64, token, request)
        
        if validlink:
            user_serializer = UserPublicSerializer(
                user, 
                context={'request': request})
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(
                {api_settings.NON_FIELD_ERRORS_KEY: [
                        "Invalid reset link"]}, 
                status=status.HTTP_400_BAD_REQUEST)
            
    def check_reset_link(self, uidb64, token, request):
        """
        Validate the reset link components
        
        Args:
            uidb64: Base 64 encoded user id
            token: Reset token specific to user
            request: rest_framework request object
            
        Returns:
            A tuple of (validlink, user) where validlink is True if reset link 
            components are valid, else False and user is a User object
            
        Raises:
            exceptions.PermissionDenied if the request uses an invalid subdomain
        """
        try:
            # urlsafe_base64_decode() decodes to bytestring on Python 3
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
                
        if user is not None and default_token_generator.check_token(user,token):
            validlink = True
        else:
            validlink = False
            
        return (validlink, user)
            

class PasswordResetConfirm(PasswordResetCheck):
    """
    Check the components of the password reset link, the base64 encoded user id
    and the user-specific token. If valid, then change the user's password to
    the provided new password.
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    ### Permissions
    * Anyone can **POST** using this endpoint.
    
    ### Fields
    Parameter       | Description                                  | Type
    --------------- | -------------------------------------------- | ---------
    `uidb64`        | Base 64 encoded user id. **Required**        | _string_
    `token`         | Reset token specific to user. **Required**   | _string_
    `new_password1` | New password. **Required**                   | _string_
    `new_password2` | New password confirmation. **Required**      | _string_
    
    ### Response
    If request is successful, an **HTTP 204**, otherwise an error message. 
        
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    """
    authentication_classes = () # No need to authenticate, as it wastes time
    permission_classes = (permissions.AllowAny,)
    serializer_class = PasswordResetConfirmSerializer
    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, )
        serializer.is_valid(raise_exception=True)
        
        uidb64 = serializer.validated_data['uidb64']
        token = serializer.validated_data['token']
                
        validlink, user = self.check_reset_link(uidb64, token, request)
        
        if validlink:
            set_password_form = SetPasswordForm(
                user=user, 
                data=serializer.validated_data)
            if set_password_form.is_valid():
                set_password_form.save()
                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(set_password_form.errors, 
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(
                {api_settings.NON_FIELD_ERRORS_KEY: [
                        "Password reset unsuccessful."]}, 
                status=status.HTTP_400_BAD_REQUEST)
        

class PasswordChange(generics.GenericAPIView):
    """
    Change password by providing a old, new, and confirmation passwords
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    ### Permissions
    * Only authenticated users can **POST** using this endpoint.
    
    ### Fields
    Parameter       | Description                             | Type
    --------------- | --------------------------------------- | ---------- 
    `old_password`  | Old password. **Required**              | _string_
    `new_password1` | New password. **Required**              | _string_
    `new_password2` | New password confirmation. **Required** | _string_
    
    ### Response
    If create is successful, an **HTTP 204**, otherwise an error message. 
        
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = PasswordChangeSerializer
    queryset = User.objects.all()
    
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        password_change_form = PasswordChangeForm(
            user=request.user, 
            data=serializer.validated_data)
        
        if password_change_form.is_valid():
            # TODO: update authentication token to log out all sessions for user
            password_change_form.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        
        else:
            return Response(password_change_form.errors, 
                            status=status.HTTP_400_BAD_REQUEST)
    
