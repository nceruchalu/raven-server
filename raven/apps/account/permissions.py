"""
Permissions for REST API
"""

from rest_framework import permissions
from raven.apps.account.models import User

class IsOwner(permissions.BasePermission):
    """
    Object-level to allow objects be only be visible and editable by the
    actual user.
    """
    
    def has_object_permission(self, request, view, obj):
        # Read/write permissions are only allowed to the request's user
        return obj.id == request.user.id

class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level to allow objects be visible to anyone, but only allow 
    actual user to edit (i.e. update/delete) it.
    """
    
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request, so we'll always allow
        # GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True
        
        # Write permissions are only allowed to the request's user
        return obj.id == request.user.id


class IsDetailOwner(permissions.BasePermission):
    """
    Global permission check to allow views to be visible only by the
    owner of the data in the detail view.
        
    Expects to be used on a detail view or a view with a lookup_field
    """
    
    def has_permission(self, request, view):
        user_owns_detail_view = False
                
        lookup_url_kwarg = view.lookup_url_kwarg or view.lookup_field
        lookup = view.kwargs.get(lookup_url_kwarg, None)
            
        if lookup is not None:
            user_owns_detail_view = request.user.email.lower() == lookup.lower()
            
        # permissions are only allowed to owner of the detail.
        return user_owns_detail_view

