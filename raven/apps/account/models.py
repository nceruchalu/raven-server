from __future__ import unicode_literals

import binascii
import datetime
import hashlib
import os
import random
import re

from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin)
from django.conf import settings
from django.core import validators
from django.core.mail import send_mail
from django.db import models, transaction
from django.utils import six, timezone
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

from rest_framework.reverse import reverse

from raven.utils.mail import send_mail_with_templates

SHA1_RE = re.compile('^[a-f0-9]{40}$')

# Create your models here.

#-------------------------------------------------------------------------------
# Custom User Model
#-------------------------------------------------------------------------------

class UserManager(BaseUserManager):
    """
    Custom UserManager for the custom AbstractUser
    """
    
    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        
        Args:
            email: user email
            password: user password
            **extra_fields: extra user model fields
            
        Returns:
            account.models.User object
         
        Raises:
            ValueError: email is not set
            ValidationError: invalid email address
        """
        # validate and normalize email
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email.lower())
        validators.validate_email(email)
        
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        
        Args:
            email: user email
            password: user password
            **extra_fields: extra user model fields
            
        Returns:
            account.models.User: regular user
         
        Raises:
            ValueError: email is not set
        """
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        Creates and saves a superuser with the given email and password.
        
        Args:
            email: user email
            password: user password
            **extra_fields: extra user model fields
            
        Returns:
            account.models.User: admin user
         
        Raises:
            ValueError: email is not set
            ValueError: Superuser must have is_staff=True
            ValueError: Superuser must have is_superuser=True
            ValidationError: invalid email address
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        
        return self._create_user(email, password, **extra_fields)

    def get_user_by_email(self, email):
        """
        Get a user that already exists in our system or create a new, inactive 
        `User` with a random password. This user has the provided email.
        This is the reason we have an `is_active` flag on the User object- so
        that we can access users on our system before they've signed up.
        
        Args:
            email: user email
                            
        Returns:
            User instance
        
        Raises:
            ValidationError: invalid email address
        """
        try:
            user = self.model.objects.get(email__iexact=email)
        except self.model.DoesNotExist:
            user = self._create_user(email, get_random_string(),is_active=False)
        return user
    
    def get_by_natural_key(self, email):
        """
        Case-insensitive lookup which is necessary for case-insensitive
        authentication
        """
        return self.get(**{'email__iexact': email})


class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    Custom User model with admin-compliant permissions.
    Django's default user doesn't work for us as we dont support username
    but instead email as the unique identifier field.
    
    Email and password are required. Other fields are optional.
    
    There is an interesting note about the context of "is_active" in this model.
        When an app user shares a file with a given email address, not all those
        contacts will be registered on the service. These unregistered
        contacts will be represented as inactive users (is_active = False). If
        these contacts eventually sign up to the service all their files and
        messages will simply be waiting for them.
    
    The following fields are inherited from the superclasses:
        * password
        * last_login
        * is_superuser
    """
    email = models.EmailField(
        _('email address'),
        unique=True,
        blank=True,
        error_messages={
            'unique': _("A user with that email already exists."),
        })
    
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_("Designates whether the user can log into this admin site.")
    )
    
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."))
    
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
        
    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def clean(self):
        super(AbstractUser, self).clean()
        self.email = self.__class__.objects.normalize_email(self.email)
        
    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class User(AbstractUser):
    """
    Concrete User class. It's an extension of AbstractUser class.
    
    Email and password are required. Other fields are optional.
    """
    
    # last modified date to be used by client apps for sync purposes.
    # ref: http://stackoverflow.com/a/5052208
    updated_at = models.DateTimeField(_('updated'), auto_now=True)
        
    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
                    
    def get_full_name(self):
        """
        Get user's full name
        If there is a valid full name then use that, otherwise use the 
        email address [prepended with an 'email:']
    
        Args:   
            None

        Returns:      
            (str) user's full name
        """
        return (super(User, self).get_full_name() or ('email:'+self.email))
    
    def __unicode__(self):
        """
        Override the representation of users to use emails
        
        Args:   
            None
            
        Returns:      
            (str) user's email address
        """
        return self.email
    
    
#-------------------------------------------------------------------------------
# Authentication Token Model
#-------------------------------------------------------------------------------

class AuthToken(models.Model):
    """
    Customized authorization token model.
    Having `key` as primary_key like rest-framework did makes it impossible to
    update it. So I'll make it unique instead
    """
    key = models.CharField(max_length=40, unique=True)
    
    user = models.OneToOneField(
        User,
        related_name='auth_token',
        on_delete=models.CASCADE)
    
    created_at = models.DateTimeField(_('created'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated'), auto_now=True)
    
    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(AuthToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return self.key


#-------------------------------------------------------------------------------
# User Registration Model
# @ref: https://github.com/macropin/django-registration
#-------------------------------------------------------------------------------
class RegistrationManager(models.Manager):
    """
    Custom Model Manager for the RegistrationProfile model.
    
    We use this as it is the preferred way to add "table-level" functionality
    to the model.
    
    The methods defined here provide shortcuts for account creation and 
    (re)activation (including generation and emailing of activation keys), and
    for cleaning out expired inactive accounts
    """
    
    def activate_user(self, activation_key):
        """
        Validate an activation key and (re)activate the corresponding `User` 
        instance if valid.
        
        To prevent reactivation of an account which has been deactivated by
        site admins, the is_activated flag is set to True
        
        Args:
            activation_key: activation key to check against.
                    
        Returns:
            Activated user if successful, or None if not these rules:
            - If the key is valid and has not expired, return the `User` after
              activating.
            - If the key is not valid or has expired, return `None`.
            - If the key is valid but the user's RegistrationProfile is already 
              active, return None
        """
        
        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
        if SHA1_RE.search(activation_key):
            try:
                registration_profile = self.select_related('user').get(
                    activation_key=activation_key)
            except self.model.DoesNotExist:
                return None
                        
            if not registration_profile.activation_key_expired():
                user = registration_profile.user
                user.is_active = True
                user.save()
                registration_profile.is_activated = True
                registration_profile.save()
                return user
        return None
            
    
    @transaction.atomic
    def get_or_create_user(self, email, password, is_active=False, 
                           send_email=True, request=None):
        """
        Get a user that already exists in our system or create a new `User`. 
        
        If this is a new user, generate an associated registration profile.
        
        If user already existed but is inactive, update the password
        and is_active flags as we would for a newly created user.
        The reason for this is that an inactive user was likely created when
        invited by another user before actual sign-up on the service.
        So we will treat this as initial user creation.
        
        Possibly email the activation key to the user.
                
        Args:
            email: user's email
            password: user's password
            is_active: Should the newly created user be active?
            send_email: flag indicator of if user gets a registration email.
            request: HttpRequest object to be passed along to the 
                email sender
                
        Returns:
            Created or updated `User` instance.
        """
        try:
            user = User.objects.get(email__iexact=email)
            if not user.is_active:
                user.is_active = is_active
                user.set_password(password)
                user.save()
            
        except User.DoesNotExist:
            user = User.objects.create_user(email, password,
                                            is_active=is_active)
        
        registration_profile = self.create_or_update_profile(user)
        
        if send_email:
            registration_profile.send_activation_email(request)
        
        return user
    
    def create_or_update_profile(self, user):
        """
        Create/Update a RegistrationProfile for a given User. 
        
        We potentially update a user's registration profile if there's a need to
        re-verify the email address of an existing user.
        
        Args:
            user: User to associate with a new or updated RegistrationProfile.
            
        Returns:
            RegistrationProfile instance
        """
        # The activation key for the RegistrationProfile will be a SHA1 hash,
        # generated from a combination of the user's email and a random salt.
        salt = hashlib.sha1(six.text_type(random.random())
                            .encode('ascii')).hexdigest()[:5]
        salt = salt.encode('ascii')
        
        email = user.email
        if isinstance(email, six.text_type):
            email = email.encode('utf-8')
        activation_key = hashlib.sha1(salt+email).hexdigest()
        
        try:
            registration_profile = self.get(user_id=user.id)
            registration_profile.activation_key = activation_key
            registration_profile.save()
            
        except self.model.DoesNotExist:
            registration_profile = self.create(
                user=user, activation_key=activation_key)
        
        return registration_profile


class RegistrationProfile(models.Model):
    """
    A simple profile which stores an activation key for use during user
    account registration/verification.
    
    Generally, you will not want to interact directly with instances of this
    model; the provided manager includes methods for creating and (re)activating
    users.
  
    """
    user = models.OneToOneField(
        User, 
        verbose_name=_('user'), 
        related_name='registration_profile',
        on_delete=models.CASCADE)
    
    activation_key = models.CharField(_('activation key'), max_length=40)
    
    is_activated =  models.BooleanField(
        _('already activated'), 
        default=False, 
        help_text=_(
            "Designates whether the user has already been activated/should "
            "not be re-activated."))
    
    created_at = models.DateTimeField(
        _('created'), 
        default=timezone.now,
        help_text=_("Registration profile creation date/time."))
    
    updated_at = models.DateTimeField(_('updated'), auto_now=True)
    
    objects = RegistrationManager()
    
    class Meta:
        verbose_name = _('registration profile')
        verbose_name_plural = _('registration profiles')
    
    def __unicode__(self):
        return u"Registration information for %s" % self.user
    
    def activation_key_expired(self):
        """
        Determine whether a RegistrationProfile's activation key has expired.
        
        Code expiration is determined by a two-step process:
            1. If the user has already been activated, the is_activated flag is
               True. Re-activating is not permitted, and so this returns True.
            
            2. Otherwise, the date the registration_profile was updated is 
               incremented by the number of days specified in
               settings.ACCOUNT_ACTIVATION_DAYS. If the result is less than or
               equal to the current date, the code has expired and this method
               returns True.
        
        Returns:
            Boolean indicator: True if key has expired, otherwise False.
        """
        expired = False
        expiration_delta = datetime.timedelta(
            days=settings.ACCOUNT_ACTIVATION_DAYS)
        return (self.is_activated == True or
                (self.updated_at + expiration_delta) <= timezone.now())
    
    def send_activation_email(self, request=None):
        """
        Send activation email to the user associated with RegistrationProfile
                
        Args:
            request: HttpRequest object to be used in determining
                the activation email link
                
        Returns:
            None
        """
        import math
        
        if self.activation_key_expired():
            return
        
        subject_template_name = 'account/activation_email_subject.txt'
        message_template_name = 'account/activation_email.txt'
        html_message_template_name ='account/activation_email.html'
                
        activation_link = reverse(
            'acct_activate', 
            request=request,
            kwargs={'activation_key': self.activation_key})
        
        # determine activation days to go
        time_since_update = timezone.now() - self.updated_at
        days_since_update = time_since_update.total_seconds()/86400.0
        expiration_days = math.ceil(
            settings.ACCOUNT_ACTIVATION_DAYS - days_since_update)
        
        context = {
            'title': "Welcome to Raven",
            'email': self.user.email,
            'activation_link': activation_link,
            'expiration_days': int(expiration_days)
            }
                
        send_mail_with_templates(
            context=context,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[self.user.email],
            subject_template_name=subject_template_name,
            message_template_name=message_template_name,
            html_message_template_name=html_message_template_name,
            request=request)
