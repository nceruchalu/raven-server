from django.contrib import admin
import django.contrib.auth.admin as auth_admin
from django.utils.translation import ugettext_lazy as _

from raven.apps.account.forms import UserChangeForm, UserCreationForm
from raven.apps.account.models import User, AuthToken, RegistrationProfile
from raven.utils.admin import ModelAdminDeleteMixin, add_link_field

# Register your models here.

class UserAdmin(auth_admin.UserAdmin, ModelAdminDeleteMixin):
    """
    Custom version of the ModelAdmin associated with the User model. It is 
    modified to work with the custom User model
    
    A custom user model requires using a custom form and add_form
    """
    # use the custom "delete selected objects" action
    actions = ['delete_selected']
    
    # These fields to be used in displaying the User model. These override the
    # definitions on the base UserAdmin that reference specific fields on
    # auth.User
    list_display =  ('email', 'first_name', 'last_name',
                     'date_joined', 'is_active',)
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined', 
                                           'updated_at')}),
        )
    
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {'classes': ('wide',),
                'fields': ('email', 'password1', 'password2'),
                }),
        )
    form = UserChangeForm
    add_form = UserCreationForm
    
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('-date_joined',)
    readonly_fields = ('last_login', 'date_joined', 'updated_at',)
    


@add_link_field(target_model='user', field='user', field_name='user_link')
class AuthTokenAdmin(admin.ModelAdmin, ModelAdminDeleteMixin):
    """
    Representation of the AuthToken model in the admin interface.
    """
    # use the custom "delete sected objects" action
    actions = ['delete_selected']
    
    list_display = ('key', 'user_link', 'created_at')
    fields = ('user', 'user_link', 'key', 'created_at')
    search_fields = ('key', 'user__email', 'user__first_name',
                     'user__last_name')
    readonly_fields = ('user', 'key', 'created_at',)
    ordering = ('-created_at',)
    
    def has_add_permission(self, request): 
        """
        Don't want users adding Auth Tokens through admin 
          
        Args:   
            request: HttpRequest object representing current request
            
        Returns:      
            (Boolean) False
        """
        return False


@add_link_field(target_model='user', field='user', field_name='user_link')
class RegistrationAdmin(admin.ModelAdmin):
    """
    Representation of the RegistrationProfile model in the admin interface.
    """
    
    actions = ['activate_users', 'resend_activation_emails']
    list_display = ('id', 'user_link', 'activation_key_expired')
    fields = ('user', 'user_link', 'activation_key', 'is_activated', 
              'updated_at')
    readonly_fields = ('user', 'activation_key', 'updated_at')
    search_fields = ('user__email', 'user__first_name', 'user__last_name')
    ordering = ('-created_at',)
    
    def has_add_permission(self, request):
        """
        No adding of RegistrationProfiles through the admin UI
        """
        return False
    
    def activate_users(self, request, queryset):
        """
        Activates the selected users, if they are not already activated.
        
        Args:
            request: HttpRequest object representing current request
            queryset: QuerySet of RegistrationProfile objects selected by admin
        """
        for registration_profile in queryset:
            RegistrationProfile.objects.activate_user(
                registration_profile.activation_key)
    activate_users.short_description = _("Activate users")

    def resend_activation_emails(self, request, queryset):
        """
        Re-sends activation emails for the selected users.

        Note that this will *only* send activation emails for users who are
        eligible to activate; emails will not be sent to users
        whose activation keys have expired or who have already activated.
        
        Args:
            request: HttpRequest object representing current request
            queryset: QuerySet of RegistrationProfile objects selected by admin
        """
        for registration_profile in queryset:
            if not registration_profile.activation_key_expired():
                registration_profile.send_activation_email(request)
    resend_activation_emails.short_description = _("Re-send activation emails")


# Register UserAdmin
admin.site.register(User, UserAdmin)
# Register AuthTokenAdmin
admin.site.register(AuthToken, AuthTokenAdmin)
# Register RegistrationAdmin
admin.site.register(RegistrationProfile, RegistrationAdmin)
