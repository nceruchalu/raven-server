"""
Provides a way of serializing and deserializing the account app model 
instances into representations such as json.
"""
from django.conf import settings
from django.contrib.auth import password_validation
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from raven.apps.account.models import User, RegistrationProfile


class AbstractBaseUserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Abstract base serializer to be used for getting and updating users.
    All other User serializers are to build off of this.
    
    Most fields are read-only. Only writeable field are: 
    - 'first_name'
    - 'last_name'
    - 'avatar'
    """
    
    # url field should lookup by 'email' not the 'pk'
    url = serializers.HyperlinkedIdentityField(
        view_name='user-detail', 
        lookup_field='email')
            
    created_at = serializers.DateTimeField(
        source='date_joined',
        label='Created', 
        read_only=True)
    
    class Meta:
        model = User
        abstract = True
    

class UserPublicSerializer(AbstractBaseUserSerializer):
    """
    Serializer class to show a privacy-respecting version of users, i.e. public
    data only and so doesn't disclose personal information like settings
    
    All fields are read-only
    """
    
    class Meta:
        model = User
        fields = ('url', 'id', 'email', 'first_name', 'last_name',
                  'is_active', 'updated_at', 'created_at')
        read_only_fields = ('id', 'email', 'first_name', 'last_name',
                            'is_active', 'updated_at', 'created_at')


class UserPrivateSerializer(AbstractBaseUserSerializer):
    """
    Serializer class to shows both public and private data.
    'first_name', 'last_name' are readwrite fields.
    """
        
    class Meta:
        model = User
        fields = ('url', 'id', 'email', 'first_name', 'last_name',
                  'is_active', 'updated_at', 'created_at',)
        read_only_fields = ('id', 'email',
                            'is_active', 'updated_at', 'created_at')


class UserCreationSerializer(AbstractBaseUserSerializer):
    """
    Serializer to be used for creating a user.
    """
    class Meta:
        model = User
        fields = ('url', 'id', 'email', 'password')
        read_only_fields = ('id',)
        extra_kwargs = {'password': {'write_only': True}}
        
    def validate_password(self, value):
        """
        Check that the password complies with the password validation settings
        
        Args:
            value: Password to be validated
            
        Returns:
            Validated value
            
        Raises:
            serializers.ValidationError: Password isn't valid
        """
        password_validation.validate_password(value)
        return value
    
    def create(self, validated_data):
        """
        Create a new user instance
        """
        # create an inactive user and send an activation email
        user = RegistrationProfile.objects.get_or_create_user(
            email=validated_data['email'],
            password=validated_data['password'],
            is_active=False,
            request=self.context['request']._request)
        
        return user


class AuthTokenSerializer(serializers.Serializer):
    """
    customized AuthTokenSerializer
    """
    email = serializers.EmailField(label=_("Email"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'})
    
    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        
        if not (email and password):
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg)
        
        return attrs


class AuthTokenVerificationSerializer(serializers.Serializer):
    """
    Serializer for verifying the authentication token of a given email
    """
    email = serializers.EmailField()
    token = serializers.CharField()


class ActivationKeySerializer(serializers.Serializer):
    """
    Serializer containing an activation key for a newly created account.
    Use this to activate an account after creation
    """
    activation_key = serializers.RegexField(
        regex=r'[a-fA-F0-9]{40}',
        write_only=True,
        error_messages={'invalid': _('Activation key in link is invalid.')})


class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    email = serializers.EmailField(write_only=True)


class PasswordResetCheckSerializer(serializers.Serializer):
    """
    Serializer for checking the password reset uid and token
    """
    uidb64 = serializers.RegexField(
        regex=r'[0-9A-Za-z_\-]+',
        write_only=True,
        error_messages={'invalid': _("uid in reset link is invalid.")},
        help_text=_("Base 64 encoded user id"))
    
    token = serializers.RegexField(
        regex=r'[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20}',
        write_only=True,
        error_messages={'invalid': _("password reset token is invalid.")},
        help_text=_("Password reset token specific to the user"))

    
class PasswordResetConfirmSerializer(PasswordResetCheckSerializer):
    """
    Serializer for checking the password reset uid and token and confirming
    the password reset by providing a new password
    """
    new_password1 = serializers.CharField(
        label=_("New password"),
        write_only=True)
    
    new_password2 = serializers.CharField(
        label=_("New password confirmation"),
        write_only=True)
    
    def validate_new_password1(self, value):
        """
        Check that the password complies with the password validation settings
        
        Args:
            value: Password to be validated
            
        Returns:
            Validated value
            
        Raises:
            serializers.ValidationError: Password isn't valid
        """
        password_validation.validate_password(value)
        return value
    

class PasswordChangeSerializer(AbstractBaseUserSerializer):
    """
    Serializer class for changing a password by providing the old password,
    new password and a confirmation of the new password.
    """
    old_password = serializers.CharField(
        label=_("Old password"),
        write_only=True)
    new_password1 = serializers.CharField(
        label=_("New password"),
        write_only=True)
    new_password2 = serializers.CharField(
        label=_("New password confirmation"),
        write_only=True)
    
    def validate_new_password1(self, value):
        """
        Check that the password complies with the password validation settings
        
        Args:
            value: Password to be validated
            
        Returns:
            Validated value
            
        Raises:
            serializers.ValidationError: Password isn't valid
        """
        password_validation.validate_password(value)
        return value
    
    class Meta:
        model = User
        fields = ('old_password', 'new_password1', 'new_password2')
