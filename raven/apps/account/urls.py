from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from raven.apps.account import views

urlpatterns = [
    # --------------------------------------------------------------------------
    # User List and Details
    # --------------------------------------------------------------------------
    # user list
    url(r'^users/$', views.UserList.as_view(), name='user-list'),
    
    # user details
    url(r'^users/(?P<email>[\w.@+-]+)/$', 
        views.UserDetail.as_view(), 
        name='user-detail'),
    
    # --------------------------------------------------------------------------
    # Authenticated User's Details
    # --------------------------------------------------------------------------
    url(r'^user/$', 
        views.AuthenticatedUserDetail.as_view(), 
        name='auth-user-detail'),
    
    # --------------------------------------------------------------------------
    # Account and Credentials Manangement endpoints
    # --------------------------------------------------------------------------
    # collection of account management endpoints
    url(r'^account/$', views.account_root, name='account_root'),
    
    # collection of password management endpoints
    url(r'^password/$', views.password_root, name='password_root'),    
    
    
    # --------------------------------------------------------------------------
    # Authentication: obtain and verify auth token
    # --------------------------------------------------------------------------
    # obtain auth token given a team, email, and password
    url(r'^account/auth/$', 
        views.ObtainExpiringAuthToken.as_view(),
        name='obtain_auth_token'),

    # verify auth token given an email
    url(r'^account/auth/verify/$', 
        views.VerifyExpiringAuthToken.as_view(),
        name='verify_auth_token'),
    
    
    # --------------------------------------------------------------------------
    # Email verification and account activation
    # --------------------------------------------------------------------------
    url(r'^account/activate/$',
        views.Activate.as_view(),
        name='acct_activate_api'),
    
    
    # --------------------------------------------------------------------------
    # Password Reset
    # --------------------------------------------------------------------------
    # request a password reset
    url(r'^password/reset/$', 
        views.PasswordReset.as_view(),
        name='acct_password_reset'),
    
    # check the password reset uid and token
    url(r'^password/reset/check/$', 
        views.PasswordResetCheck.as_view(),
        name='acct_password_reset_check'),
      
    # check the password reset uid and token and if valid reset password
    url(r'^password/reset/confirm/$', 
        views.PasswordResetConfirm.as_view(),
        name='acct_password_reset_confirm_api'),
       
    
    # --------------------------------------------------------------------------
    # Password Change
    # --------------------------------------------------------------------------
    url(r'^password/change/$',
        views.PasswordChange.as_view(),
        name='acct_password_change'),
    ]

urlpatterns = format_suffix_patterns(urlpatterns)
