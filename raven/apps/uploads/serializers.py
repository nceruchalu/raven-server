"""
Provides a way of serializing and deserializing the uploads app model 
instances into representations such as json.
"""

from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from raven.apps.uploads.models import (
    Upload, File, get_upload_archive_path, get_file_file_path)


class FileSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer to be used for getting and updating Files.
    """
    url = serializers.HyperlinkedIdentityField(
        view_name='file-detail', 
        lookup_field='hash_key')

    uploaded_file = serializers.BooleanField(
        label=_("Has the file been uploaded?"),
        default=False,
        write_only=True)
    
    class Meta:
        model = File
        fields = ('url', 'id', 'hash_key', 'name', 'size', 'content_type',
                  'file', 'created_at', 'updated_at', 'uploaded_file',)
        read_only_fields = ('url', 'hash_key', 'name', 'size', 'content_type',
                            'file', 'created_at', 'updated_at')

    def update(self, instance, validated_data):
        uploaded_file = validated_data.get('uploaded_file', False)
        
        if uploaded_file:
            instance.file = get_file_file_path(instance, instance.name)
            instance.save()
            
        return instance

    
class FileCreationSerializer(FileSerializer):
    """
    Serializer to be used for creating a file.
    """
    class Meta:
        model = File
        fields = ('url', 'id', 'hash_key', 'name', 'size', 'content_type',
                  'created_at', 'updated_at')
        read_only_fields = ('url', 'hash_key', 'created_at', 'updated_at')
    
    def create(self, validated_data):
        """
        Create a new file instance
        """
        request = self.context['request']
        instance = File.objects.create_file(
            name=validated_data['name'],
            size=validated_data['size'],
            content_type=validated_data['content_type'],
            request=request._request)
        
        return instance


class FileHashkeySerializer(serializers.Serializer):
    """
    Serializer to be used for identifying a file while creating an upload.
    """
    hash_key = serializers.CharField(
        label=_("Unique hash"),
         write_only=True,
        help_text=_("Unique identifier of file."))
    

class UploadSerializer(serializers.ModelSerializer):
    """
    Serializer to be used for getting and updating Uploads.
    """
    url = serializers.HyperlinkedIdentityField(
        view_name='upload-detail', 
        lookup_field='hash_key')

    uploaded_archive = serializers.BooleanField(
        label=_("Has the archive been uploaded?"),
        default=False,
        write_only=True)
    
    class Meta:
        model = Upload
        fields = ('url', 'id', 'hash_key', 'files_count', 'archive',
                  'uploaded_archive', 'created_at', 'updated_at')
        read_only_fields = ('url', 'hash_key', 'files_count', 'archive',
                            'created_at', 'updated_at')

    def update(self, instance, validated_data):
        uploaded_archive = validated_data.get('uploaded_archive', False)
        
        if instance.files_count > 1 and not instance.archive:
            if uploaded_archive:
                # assume the archive file now exists
                instance.archive = get_upload_archive_path(instance,"raven.zip")
                instance.save()
            else:
                # time to set-off a task to create the archive file
                instance.create_archive()
                
        return instance


class UploadCreationSerializer(serializers.Serializer):
    """
    Serializer for creating uploads
    """
    files = FileHashkeySerializer(many=True, required=True)
    has_archive = serializers.BooleanField(
        label=_("has archive"),
        default=False,
        write_only=True,
        help_text=_("Does the client have an archive they would "
                    "like to upload?"))
    
    class Meta:
        extra_kwargs = {'files': {'write_only': True}}

    def create(self, validated_data):
        """
        Create a new Upload instance
        """
        hash_keys = [f['hash_key'] for f in validated_data.get('files', [])]
                
        # get Files in list that haven't been previously linked to other
        # uploads
        files = File.objects.filter(hash_key__in=hash_keys, upload=None)
        if not files:
            raise serializers.ValidationError("Specified files don't exist")

        # Get request's IP address
        request = self.context['request']
        ip_address = request._request.META.get("REMOTE_ADDR", None)
        
        # Create upload object and link files to container
        upload = Upload.objects.create(files_count=len(files),
                                       ip_address=ip_address)
        upload.files.add(*files)
        
        return upload


class RecipientSerializer(serializers.Serializer):
    """
    Serializer to be used for identifying a recipient while creating an upload.
    """
    email = serializers.EmailField(
        label=_("email"),
         write_only=True,
        help_text=_("Unique identifier of recipient."))
    

class UploadForwardSerializer(serializers.Serializer):
    """
    Serializer for forwarding uploads
    """
    emails = RecipientSerializer(many=True, required=True)
    
    class Meta:
        extra_kwargs = {'emails': {'write_only': True}}
    
