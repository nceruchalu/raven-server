from django.contrib import admin
from django.utils.html import mark_safe
from django.utils.translation import ugettext_lazy as _

from raven.apps.uploads.models import Upload, File
from raven.utils.admin import ModelAdminDeleteMixin, add_link_field

# Register your models here.

@add_link_field(target_model='user', field='owner', app='account', 
                field_name='owner_link', short_description="Owner link")
class UploadAdmin(admin.ModelAdmin, ModelAdminDeleteMixin):
    """
    Representation of an upload in the admin UI
    """
    # use the custom "delete selected objects" action
    actions = ['delete_selected']
    
    list_display = ('hash_key', 'files_count', 'download_file_link',
                    'created_at')
    list_select_related = ('owner',)
    date_hierarchy = 'created_at'
    fieldsets = (
        (None, {'fields': ('hash_key',)}),
        (_('Details'), {'fields': ('files_count', 'ip_address')}),
        (_('Owner'), {'fields': ('owner', 'owner_link')}),
        (_('Archive'), {'fields': ('archive', 'download_file_link',)}),
        (_('Timestamps'), {'fields': ('created_at', 'updated_at')}),
    )
    search_fields = ('hash_key', 'owner__email',)
    ordering = ('-created_at',)
    readonly_fields = ('owner', 'hash_key', 'files_count', 'ip_address',
                       'archive', 'download_file_link',
                       'created_at',  'updated_at')

    def download_file_link(self, instance):
        """
        Download file wrapped in a link like any other FileField would
        """
        if instance.download_file:
            url = instance.download_file.url.encode('utf-8')
            url_text = instance.download_file.name
        else:
            url = "javascript:;"
            url_text = "None"
        return mark_safe(
            '<a href="{url}" target="_blank">{url_text}</a>'.format(
                url=url, url_text=url_text))
        pass
    download_file_link.allow_tags = True
    download_file_link.short_description = 'Download file'
    
    
@add_link_field(target_model='user', field='owner', app='account', 
                field_name='owner_link', short_description="Owner link")
@add_link_field(target_model='upload', field='upload', field_name='upload_link')
class FileAdmin(admin.ModelAdmin, ModelAdminDeleteMixin):
    """
    Representation of a file in the admin UI
    """
    # use the custom "delete selected objects" action
    actions = ['delete_selected']
    
    list_display = ('hash_key', 'name', 'size', 'content_type', 'file',
                    'upload_link', 'created_at',)
    list_select_related = ('owner', 'upload',)
    date_hierarchy = 'created_at'
    fieldsets = (
        (None, {'fields': ('hash_key',)}),
        (_('Details'), {'fields': ('name', 'size', 'content_type', 'file',
                                   'ip_address')}),
        (_('Upload'), {'fields': ('owner', 'owner_link',
                                  'upload', 'upload_link',)}),
        (_('Timestamps'), {'fields': ('created_at', 'updated_at')}),
        )
    search_fields = ('name', 'hash_key', 'upload__hash_key', 'owner__email',)
    ordering = ('-created_at',)
    readonly_fields = ('owner', 'upload', 'hash_key', 'name', 'size',
                       'content_type', 'file', 'ip_address',
                       'created_at', 'updated_at')

    def has_add_permission(self, request):
        """
        If you want to add a file, do this through the front-end
        """
        return False
    

# Regsiter UploadAdmin
admin.site.register(Upload, UploadAdmin)
# Register FileAdmin
admin.site.register(File, FileAdmin)
