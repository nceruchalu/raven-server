"""
Permissions for REST API
"""

from rest_framework import permissions

class IsProcessingOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow non-safe methods on uploads still
    processing (creating the archive)
    """
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request, so we'll always allow
        # GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True
    
        # Update only allowed on uploads that are awaiting an archive file
        if request.method in ['PATCH', 'PUT']:
            return obj.files_count > 1 and not obj.archive

        # Can't delete an upload via the API
        return False

    
class IsUploadingOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow non-safe methods on files still
    uploading
    """
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request, so we'll always allow
        # GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True
    
        # Update/Delete only allowed on files that haven't been fully uploaded
        # yet
        return not (obj.file and obj.upload)
