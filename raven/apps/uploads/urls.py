from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from raven.apps.uploads import views

urlpatterns = [
    # --------------------------------------------------------------------------
    # Upload Management
    # --------------------------------------------------------------------------
    # start upload creation
    url(r'^uploads/$', views.UploadList.as_view(), name='upload-list'),

    # upload details
    url(r'^uploads/(?P<hash_key>[\w]+)/$',
        views.UploadDetail.as_view(),
        name='upload-detail'),

    # forward upload details to an email address
    url(r'^uploads/(?P<hash_key>[\w]+)/forward/$',
        views.UploadDetailForward.as_view(),
        name='upload-detail-forward'),
    
    # --------------------------------------------------------------------------
    # File Management
    # --------------------------------------------------------------------------
    # start file upload
    url(r'^files/$', views.FileList.as_view(), name='file-list'),

    # file details
    url(r'^files/(?P<hash_key>[\w]+)/$',
        views.FileDetail.as_view(),
        name='file-detail'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
