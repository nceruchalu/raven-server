import os

from django.conf import settings
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import View

from rest_framework import generics, permissions, status, response
from rest_framework.reverse import reverse

from raven.apps.aws.utils import get_s3_client
from raven.apps.uploads.models import (
    Upload, File, get_upload_archive_path, get_file_file_path)
from raven.apps.uploads.permissions import (
    IsProcessingOrReadOnly, IsUploadingOrReadOnly)
from raven.apps.uploads.serializers import (
    UploadCreationSerializer, UploadSerializer, UploadForwardSerializer,
    FileCreationSerializer, FileSerializer)
from raven.utils.base import human_readable_size
from raven.utils.mail import send_mail_with_templates


# Create your views here.
# --------------------------------------------------------------------------
# UPLOAD MANAGEMENT
# --------------------------------------------------------------------------
class UploadList(generics.CreateAPIView):
    """
    Create a stub upload object and get signed S3 credentials for client-side
    upload
    
    ## Reading
    You can't read using this endpoint
    
    
    ## Publishing
    ### Permissions
    * Anyone can __POST__ using this endpoint.
    
    ### Fields
    Parameter      | Description                                    | Type
    -------------- | ---------------------------------------------- | ---------
    `files`        | Files tied to upload where each entry is a     | _list_
                   | _dict_ with only key of `hash_key`             |
    `has_archive`  | Does the client have an archive to upload?     | _boolean_
    
    
    ### Response
    If create is successful, an upload object
    
    Name             | Description                                  | Type
    ---------------- | -------------------------------------------- | ---------
    `url`            | URL of upload object                         | _string_
    `id`             | A unique identifier of the file              | _string_
    `hash_key`       | A unique identifier of the file              | _string_
    `files_count`    | Count of number of files in this upload      | _integer_
    `archive`        | Absolute URL to uploaded archived file       | _string_
    `created_at`     | Upload's creation date/time                  | _datetime_
    `updated_at`     | Upload's last update date/time               | _datetime_
    `meta`           | An object containing additional metadata     | _dict_
    
    
    ##### The file metadata object contains fields
    
    Name              | Description                                 | Type
    ----------------- | ------------------------------------------- | ----------
    `presigned_post`  | Amazon S3 temporary credentials signature   | _dict_
                      | for direct upload of an archive file to a   |
                      | bucket's key                                |
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    
    
    ## Endpoints
    Name                                     | Description                
    ---------------------------------------- | ---------------------------------
    [`hash_key/`](hash_key/)                 | Get/update an upload's details
    [`hash_key/forward/`](hash_key/forward/) | Forward an upload via email
    
    ##
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = UploadCreationSerializer
    queryset = Upload.objects.all()

    def create(self, request, *args, **kwargs):
        """
        Augment serializer data with S3 signed request parameters
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        instance = serializer.instance
        response_serializer = UploadSerializer(instance,
                                               context={'request': request})
        serializer_data = response_serializer.data

        # Attach the presigned post to the serializer_data if the client
        # wants to attempt it and there's a need to do so (more than 1 file)
        has_archive = serializer.validated_data['has_archive']
        if instance.files_count > 1:
            if has_archive:
                presigned_post = self.generate_presigned_post(instance)
                serializer_data['meta'] = {'presigned_post': presigned_post}
            else:
                # client has no archive to upload and we need one so attempt
                instance.create_archive()
                
        headers = self.get_success_headers(serializer_data)
        return response.Response(serializer_data,
                                 status=status.HTTP_201_CREATED,
                                 headers=headers)
    
    def generate_presigned_post(self, instance):
        # Get an S3 client
        s3_client = get_s3_client()
        file_path = get_upload_archive_path(instance, "raven.zip")
        file_name = os.path.basename(file_path)
                
        # All uploaded files should be downloaded by browsers so we have to tell
        # the browser to treat the response as a file attachment. We do this by
        # setting the Content-Disposition header appropriately.
        content_disposition = 'attachment; filename="{name}"'.format(
            name=file_name)
        
        # Only specify a content type in the presigned post fields and
        # conditions if one has been provided
        s3_fields = {
            'acl': "public-read",
            'Content-Disposition': content_disposition,
            'Cache-Control': "public, max-age=31536000, s-maxage=31536000",
            'Content-Type': "application/zip"
        }
        
        s3_conditions = [{'acl': "public-read"},
                         ["starts-with", "$Content-Disposition", ""],
                         ["starts-with", "$Cache-Control", ""],
                         ["starts-with", "$Content-Type", ""]]
        
        # Generate presigned post for the file upload
        presigned_post = s3_client.generate_presigned_post(
            Bucket=settings.AWS_STORAGE_BUCKET_NAME,
            Key=file_path,
            Fields = s3_fields,
            Conditions = s3_conditions,
            ExpiresIn=3600
        )

        return presigned_post


class UploadDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Get/update an upload
    
    ## Reading
    ### Permissions
    * Anyone can read this endpoint.
    
    ### Fields
    Reading this endpoint returns an upload object
    
    Name             | Description                                  | Type
    ---------------- | -------------------------------------------- | ---------
    `url`            | URL of upload object                         | _string_
    `id`             | A unique identifier of the file              | _string_
    `hash_key`       | A unique identifier of the file              | _string_
    `files_count`    | Count of number of files in this upload      | _integer
    `archive`        | Absolute URL to uploaded archived file       | _string_
    `created_at`     | Upload's creation date/time                  | _datetime_
    `updated_at`     | Upload's last update date/time               | _datetime_
    
    
    ## Publishing
    You can't create using this endpoint
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    ### Permissions
    * Anyone can write to this endpoint, while there isn't an archive file, if
      one is needed
    
    ### Fields
    Parameter          | Description                                | Type
    ------------------ | ------------------------------------------ | ----------
    `uploaded_archive` | Indicator that archive has been uploaded   | _boolean_
    
    ### Response
    If update is successful, an upload object, otherwise an error message.
    
    
    ## Endpoints
    Name                                     | Description                
    ---------------------------------------- | ---------------------------------
    [`forward/`](forward/)                   | Forward upload via email
    
    ##
    """
    permission_classes = (permissions.AllowAny,
                          IsProcessingOrReadOnly)
    serializer_class = UploadSerializer
    queryset = Upload.objects.all()
    
    # lookup by 'hash_key' not the 'pk' but allow for case-insensitive lookups
    lookup_field = 'hash_key__iexact'
    lookup_url_kwarg = 'hash_key'


class UploadDetailForward(generics.UpdateAPIView):
    """
    Forward an upload's details by emailing it to the requested recipient(s).
    
    ## Reading
    You can't read using this endpoint.
    
    
    ## Publishing
    You can't create using this endpoint.
    
    
    ## Deleting
    You can't delete using this endpoint.
    
    
    ## Updating
    ### Permissions
    * Anyone can update using this endpoint.
    
    ### Fields
    Parameter    | Description                                       | Type
    ------------ | ------------------------------------------------- | --------
    `emails`     | List of email specification objects where each    | _list_
                 | object has one key _email_ with a string as value |
    
    ### Response
    If update is successful, a success message, otherwise an error message.
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = UploadForwardSerializer
    queryset = Upload.objects.all()
    
    # lookup by 'hash_key' not the 'pk' but allow for case-insensitive lookups
    lookup_field = 'hash_key__iexact'
    lookup_url_kwarg = 'hash_key'

    def update(self, request, *args, **kwargs):
        """
        Override the superclass implementation of update to not call
        serializer.save()
        """
        partial = kwargs.pop('partial', False)
        upload = self.get_object()
        serializer = self.get_serializer(data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        emails_data = serializer.validated_data.get('emails', [])
        emails = [e['email'] for e in emails_data]
        
        # get all linked files
        total_size = 0
        files = []
        for f in upload.files.values_list('size', 'name'):
            total_size += f[0]
            files.append(f[1])

        # get the download link
        download_link = reverse(
            'upload_link',
            request=request,
            kwargs={'hash_key': upload.hash_key})
        
        subject_template_name = 'uploads/forward_email_subject.txt'
        message_template_name = 'uploads/forward_email.txt'
        html_message_template_name ='uploads/forward_email.html'

        context = {
            'title': "You have been sent a file via Raven",
            'download_link': download_link,
            'total_size': human_readable_size(total_size),
            'files': files
        }

        for email in emails:
            context['email'] = email
            send_mail_with_templates(
                context=context,
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipient_list=[email],
                subject_template_name=subject_template_name,
                message_template_name=message_template_name,
                html_message_template_name=html_message_template_name,
                request=request._request)
        
        return response.Response({'detail': True})

    
# --------------------------------------------------------------------------
# FILE MANAGEMENT
# --------------------------------------------------------------------------
class FileList(generics.CreateAPIView):
    """
    Create a stub file object and get signed S3 credentials for client-side
    upload
    
    ## Reading
    You can't read using this endpoint
    
    
    ## Publishing
    ### Permissions
    * Anyone can __POST__ using this endpoint.
    
    ### Fields
    Parameter      | Description                                    | Type
    -------------- | ---------------------------------------------- | ---------
    `name`         | File's name                                    | _string_
    `size`         | File's size, in bytes                          | _string_
    `content_type` | File's content-type header                     | _string_
    
    ### Response
    If create is successful, a file object
    
    Name             | Description                                  | Type
    ---------------- | -------------------------------------------- | ---------
    `url`            | URL of file object                           | _string_
    `id`             | A unique identifier of the file              | _string_
    `hash_key`       | A unique identifier of the file              | _string_
    `name`           | File's base filename                         | _string_
    `size`           | File's size, in bytes                        | _string_
    `content_type`   | File's content-type header                   | _string_
    `created_at`     | File's creation date/time                    | _datetime_
    `updated_at`     | File's last update date/time                 | _datetime_
    `meta`           | An object containing additional metadata     | _dict_
    
    ##### The file metadata object contains fields
    
    Name              | Description                                  | Type
    ----------------- | -------------------------------------------- | -------
    `presigned_post`  | Amazon S3 temporary credentials signature    | _dict_
                      | for direct upload of file to a bucket's key  |
    
    
    ## Deleting
    You can't delete using this endpoint
    
    
    ## Updating
    You can't update using this endpoint
    
    
    ## Endpoints
    Name                           | Description                
    ------------------------------ | -------------------------------------------
    [`hash_key/`](hash_key/)       | Get/update/delete a specific file's details
    
    ##
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = FileCreationSerializer
    queryset = File.objects.all()

    def create(self, request, *args, **kwargs):
        """
        Augment serializer data with S3 signed request parameters
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        serializer_data = serializer.data
        instance = serializer.instance
        
        # Get an S3 client
        s3_client = get_s3_client()
        
        file_path = get_file_file_path(instance, instance.name)
                
        # All uploaded files should be downloaded by browsers so we have to tell
        # the browser to treat the response as a file attachment. We do this by
        # setting the Content-Disposition header appropriately.
        content_disposition = 'attachment; filename="{name}"'.format(
            name=instance.name)
        
        # Only specify a content type in the presigned post fields and
        # conditions if one has been provided
        s3_fields = {
            'acl': "public-read",
            'Content-Disposition': content_disposition,
            'Cache-Control': "public, max-age=31536000, s-maxage=31536000"
        }
        
        s3_conditions = [{'acl': "public-read"},
                         ["starts-with", "$Content-Disposition", ""],
                         ["starts-with", "$Cache-Control", ""]]
        
        if instance.content_type:
            s3_fields['Content-Type'] = instance.content_type,
            s3_conditions.append({'Content-Type': instance.content_type})
        
        # Generate presigned post for the file upload
        presigned_post = s3_client.generate_presigned_post(
            Bucket=settings.AWS_STORAGE_BUCKET_NAME,
            Key=file_path,
            Fields = s3_fields,
            Conditions = s3_conditions,
            ExpiresIn=3600
        )
        
        # Attach the presigned post to the serializer_data
        serializer_data['meta'] = {'presigned_post': presigned_post}
        
        headers = self.get_success_headers(serializer_data)
        return response.Response(serializer_data,
                                 status=status.HTTP_201_CREATED,
                                 headers=headers)


class FileDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Get/update/delete a file
    
    ## Reading
    ### Permissions
    * Anyone can read this endpoint.
    
    ### Fields
    Reading this endpoint returns a file object
    
    Name             | Description                                  | Type
    ---------------- | -------------------------------------------- | ---------
    `url`            | URL of file object                           | _string_
    `id`             | A unique identifier of the file              | _string_
    `hash_key`       | A unique identifier of the file              | _string_
    `name`           | File's base filename                         | _string_
    `size`           | File's size, in bytes                        | _string_
    `content_type`   | File's content-type header                   | _string_
    `file`           | Absolute URL to uploaded file object         | _string_
    `archive`        | Absolute URL to uploaded archived file       | _string_
    `created_at`     | File's creation date/time                    | _datetime_
    `updated_at`     | File's last update date/time                 | _datetime_
    
    
    
    ## Publishing
    You can't create using this endpoint
    
    
    ## Deleting
    ### Permissions
    * Anyone can delete using this endpoint, while there isn't an associated
      upload.
      
    ### Response
    If successful, a 204 status code, else an error message.
    
    
    ## Updating
    ### Permissions
    * Anyone can write to this endpoint, while there isn't an associated
      upload.
    
    ### Fields    
    Parameter          | Description                                | Type
    ------------------ | ------------------------------------------ | ----------
    `uploaded_file`    | Indicator that the file has been uploaded  | _boolean_
    `uploaded_archive` | Indicator that archive has been uploaded   | _boolean_
    
    ### Response
    If update is successful, a file object, otherwise an error message.
    """
    permission_classes = (permissions.AllowAny,
                          IsUploadingOrReadOnly)
    serializer_class = FileSerializer
    queryset = File.objects.all()
    
    # lookup by 'hash_key' not the 'pk' but allow for case-insensitive lookups
    lookup_field = 'hash_key__iexact'
    lookup_url_kwarg = 'hash_key'
    


# -----------------------------------------------------------------------------
# WEB VIEWS
# -----------------------------------------------------------------------------

class UploadView(View):
    """
    Download file linked to upload object
    """
    def get(self, request, *args, **kwargs):
        # get the upload
        hash_key = kwargs.get('hash_key')
        upload = get_object_or_404(Upload, hash_key__iexact=hash_key)
        download_file = upload.download_file
        if download_file:
            return redirect(download_file.url)
        else:
            # TODO: check if the archive is being created and if so report this
            # so user knows to check back in a bit. Also track which user
            # clicked on this and email them back to let them know it's now
            # ready
            raise Http404
            
