from __future__ import unicode_literals

import os
import re
from tempfile import TemporaryFile
import uuid
import zipfile

from django.core.validators import MinValueValidator
from django.db import models
from django.core.files.storage import DefaultStorage
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

import requests

from raven.apps.account.models import User
from raven.utils.base import get_upload_path

# Create your models here.

# ------------------------------------------------------------------------------
# Constants
# ------------------------------------------------------------------------------
HASH_LENGTH = 20 # max length of generated hash keys


# ------------------------------------------------------------------------------
# HELPER FUNCTIONS
# ------------------------------------------------------------------------------

def get_upload_archive_path(instance, filename):
    """
    Upload path for an upload file's archive file
    
    Args:
        instance: Upload model instance where file is being attached
        filename: filename that was originally given to the file
    
    Returns: 
        Unique filepath for given file, that's a subpath of the root path
    """
    root = "upload/u/{hash_key}/".format(hash_key=instance.hash_key)
    basename = "raven-{unique}".format(unique=instance.hash_key[:6])
    return get_upload_path(instance, filename, root, basename=basename)


def get_file_file_path(instance, filename):
    """
    Upload path for an upload file's original file while preserving as much
    of the filename as possible. Still has to comply with AWS S3's list of
    safe characters
    
    Args:
        instance: File model instance where file is being attached
        filename: filename that was originally given to the file
    
    Returns: 
        Unique filepath for given file, that's a subpath of the root path
    
    Ref:
        http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingMetadata.html#object-key-guidelines-safe-characters
    """
    name = os.path.basename(filename)
    # Replace all runs of whitespace with a single underscore
    safe_name = re.sub(r'\s+', '_', name)
    # Strip all non-safe characters
    safe_name = re.sub(r'[^a-zA-Z0-9!_\-\.\*\'\(\)]', "", safe_name)
    
    return "upload/f/{hash_key}/{name}".format(
        hash_key=instance.hash_key,
        name=safe_name)


# ------------------------------------------------------------------------------
# MODEL CLASSES
# ------------------------------------------------------------------------------

class Upload(models.Model):
    """
    Upload folder class which is the container class of the linked upload files.
    """
    owner = models.ForeignKey(
        User,
        verbose_name=_('owner'),
        related_name='uploads',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text=_("User who created this upload."))

    hash_key = models.CharField(
        _('unique hash'),
        max_length=40,
        unique=True,
        help_text=_("Unique identifier of upload."))

    archive = models.FileField(
        _('archive'),
        upload_to=get_upload_archive_path,
        max_length=100,
        blank=True,
        help_text=_("Associated archive file that's transmitted when upload "
                    "has multiple files."))
    
    files_count = models.IntegerField(
        _('files count'),
        default=0,
        validators=[MinValueValidator(0)],
        help_text=_("Number of files in this upload."))

    ip_address = models.GenericIPAddressField(
        _('IP address'), 
        unpack_ipv4=True, 
        blank=True, 
        null=True,
        help_text=_("IP address used for creating the upload."))
        
    created_at = models.DateTimeField(_('created'), default=timezone.now)
    
    updated_at = models.DateTimeField(_('updated'), auto_now=True)

    def __unicode__(self):
        return self.hash_key

    @property
    def download_file(self):
        """
        The file that's to be downloaded. If this upload has just 1 file,
        it's that file's original file, otherwise it's the upload's archive
        """
        from raven.apps.uploads.models import File

        download = self.archive
        
        if self.files_count == 1:
            try:
                single_file = self.files.all()[:1].get()
                download = single_file.file
            except File.DoesNotExist:
                pass
            
        return download
        
    def generate_hash_key(self):
        """
        Generate unique string to be used as hash key for Upload object
        
        Returns:
            Unique hash key
        """
        # Generate the hash key
        hash_key = unicode(uuid.uuid4()).replace('-','')[:HASH_LENGTH]
        
        # Confirm it's unique
        count = Upload.objects.filter(hash_key=hash_key).count()
        if count >= 1:
            # hash key isn't unique so append the count to it
            hash_key = hash_key + str(count)
        return hash_key

    def create_archive(self):
        """
        Create the archive file by downloading all linked files and linking them
        together
        """
        if not self.archive:
            # only bother proceeding if there isn't an archive.
            # first check that the file doesnt exist and client failed to
            storage = DefaultStorage()
            archive_path = get_upload_archive_path(self, "raven.zip")
            if storage.exists(archive_path):
                # The file already exists. So all we need is a database update
                self.archive = archive_path
                self.save()
            else:
                # The file doesn't exist, so this calls for fetching all the
                # files and zipping them
                temp_file = TemporaryFile()
                zip_file = zipfile.ZipFile(temp_file, 'w')
                for subfile in self.files.all():
                    if subfile.file:
                        response = requests.get(subfile.file.url)
                        if (response and
                            response.status_code == requests.codes.ok):
                            zip_file.writestr(subfile.name, response.content)
                zip_file.close()
                self.archive.save("raven.zip", temp_file)
                

    def save(self, *args, **kwargs):
        """
        On instance save ensure:
        - generate hash key on object creation
        
        Args:   
            *args: all positional arguments
            **kwargs: all keyword arguments

        Returns:
            None 
        """
        if self.pk is None:
            # new upload so generate hash key
            self.hash_key = self.generate_hash_key()
        
        super(Upload, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """
        Default model delete() doesn't delete files on storage, so force that to
        happen.
        
        Args:
            *args: all positional arguments
            **kwargs: all keyword arguments

        Returns:
            None 
        """
        # delete archived file
        if self.archive:
            self.archive.delete(False)

        # delete linked files to ensure proper storage cleanup happens
        for file in self.files.all():
            file.delete()
            
        super(Upload, self).delete(*args, **kwargs)


class FileManager(models.Manager):
    """
    Custom Model Manager for the File model.
    
    We use this as it is the preferred way to add "table-level" functionality
    to the model.
    
    The methods defined here provide shortcuts for account creation and 
    (re)activation (including generation and emailing of activation keys), and
    for cleaning out expired inactive accounts
    """
    def create_file(self, name, size, content_type, request=None,owner=None):
        """
        Creates and saves a File object with the given details, as well
        as fill in the final URL for the actual file field.
        
        Args:
            name: file name
            size: file size (in bytes)
            content_type: File's content-type header value
            request: HttpRequest object
            owner: User object
        
        Returns:
            File object
        """
        hash_key = self.generate_hash_key()
        ip_address = (request.META.get("REMOTE_ADDR", None)
                      if request else None)
        instance = File(owner=owner,
                        hash_key=hash_key,
                        name=name,
                        size=size,
                        content_type=content_type,
                        ip_address=ip_address)
        
        instance.save()
        return instance

    def generate_hash_key(self):
        """
        Generate unique string to be used as hash key for a File object
        
        Returns:
            Unique hash key
        """
        # Generate the hash key
        hash_key = unicode(uuid.uuid4()).replace('-','')[:HASH_LENGTH]

        # Confirm it's unique
        count = self.filter(hash_key=hash_key).count()
        if count >= 1:
            # hash key isn't unique so append the count to it
            hash_key = hash_key + str(count)
        return hash_key
        

class File(models.Model):
    """
    Uploaded file representation
    """
    owner = models.ForeignKey(
        User,
        verbose_name=_('owner'),
        related_name='files',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text=_("User who created this file."))
    
    upload = models.ForeignKey(
        Upload,
        verbose_name=_('upload'),
        related_name='files',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        help_text=_("Upload session this file's linked to."))
    
    hash_key = models.CharField(
        _('unique hash'),
        max_length=40,
        unique=True,
        help_text=_("Unique identifier of file."))

    name = models.CharField(
        _('name'),
        max_length=250,
        help_text=_("File name."))

    size = models.IntegerField(
        _('size'),
        default=0,
        validators=[MinValueValidator(0)],
        help_text=_("File size, in bytes."))

    content_type = models.CharField(
        _('content type'),
        max_length=255,
        blank=True,
        help_text=_("File's content-type header."))
    
    file = models.FileField(
        _('file'),
        upload_to=get_file_file_path,
        max_length=300,
        blank=True,
        help_text=_("Associated file."))

    ip_address = models.GenericIPAddressField(
        _('IP address'),
        unpack_ipv4=True,
        blank=True,
        null=True,
        help_text=_("IP address used for creating the file."))
    
    created_at = models.DateTimeField(_('created'), default=timezone.now)
    
    updated_at = models.DateTimeField(_('updated'), auto_now=True)

    objects = FileManager()

    def __unicode__(self):
        return self.hash_key

    def save(self, *args, **kwargs):
        """
        On instance save ensure:
        - generate hash key on object creation
        
        Args:   
            *args: all positional arguments
            **kwargs: all keyword arguments

        Returns:
            None 
        """
        if self.pk is None:
            if not self.hash_key:
                self.hash_key = self.objects.generate_hash_key()
            
        super(File, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """
        Default model delete() doesn't delete files on storage, so force that to
        happen.
        
        Args:
            *args: all positional arguments
            **kwargs: all keyword arguments

        Returns:
            None 
        """
        # delete original and archived files
        if self.file:
            self.file.delete(False)
        
        super(File, self).delete(*args, **kwargs)
