from django.conf.urls import url

from raven.apps.uploads import views

urlpatterns = [
    # redirect to actual download file
    url(r'^d/(?P<hash_key>\w+)/$',
        views.UploadView.as_view(),
        name='upload_link'),
]
