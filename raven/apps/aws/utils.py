"""
Utilities for working with AWS
"""
import threading

import boto3
from django.conf import settings

# boto3 recommends per-thread sessions. We store them in
# THREAD_LOCAL.boto_session
THREAD_LOCAL = threading.local()


def get_s3_session():
    """
    Get an S3 session using the recommendation to create each instance in a
    multi-threaded application rather than sharing a single instance among
    the threads.
    
    Note:
        We handle the fact that the session object, particularly its
        initializing is not thread-safe and results in exceptions like:
            KeyError: 'endpoint_resolver'
    
    Ref:
        http://boto3.readthedocs.io/en/latest/guide/resources.html?highlight=threaded#multithreading
        https://github.com/boto/botocore/issues/577
    """
    session = getattr(THREAD_LOCAL, 'boto_session', None)
    if session is None:
        session = boto3.session.Session()
        THREAD_LOCAL.boto_session = session
    return session


def get_s3_client():
    """
    Get an S3 client using the session for the corresponding thread
    """
    session = get_s3_session()
    return session.client(
        's3',
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
        use_ssl=True)
