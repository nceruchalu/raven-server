"""
Custom middleware
"""
import json
import logging
import time

from django.conf import settings

from rest_framework.authentication import get_authorization_header

from raven.utils.log import clean_request_data

class LoggingMiddleware(object):
    """
    Middleware that logs request data
    """
    def __init__(self, get_response):
        self.get_response = get_response
        
    def __call__(self, request):
        # Code to be executed for each request before the view
        # (and later middleware) are called
        self.process_request(request)
        
        response = self.get_response(request)
        
        # Code to be executed for each request/response after the view is called
        response = self.process_response(request, response)
        
        return response
        
    def process_request(self, request):
        """
        Args:
            request: HttpRequest object
            
        Returns:
            None, which allows Django to continue processing this request and
            execute other process_request() middleware.
        """        
        # Cache request start time
        request.start_time = time.time()
        
        # Cache request body before it's read and unable to be extracted
        try:
            request_data = json.loads(request.body)
        except ValueError:
            request_data = request.body
        request_data = clean_request_data(request_data)
        request.logging_data = request_data
        
    def process_response(self, request, response):
        """
        Args:
            request: HttpRequest object
            response: HttpResponse or StreamingHttpResponse object returned by
                a Django view or by a middleware.
            
        Returns:
            The passed in response object as is
        """
        # Get the request data
        try:
            request_data = request.logging_data
        except AttributeError:
            request_data = None
        
        request_context = {
            'path': request.path,
            'method': request.method,
            'data': request_data,
            'query_params': request.GET,
            'authorization_header': get_authorization_header(request) or None
            }
        
        # Get the response data
        if response.streaming:
            response_context = "<<<Streaming>>>"
            
        else:
            status_code = response.status_code
            if status_code < 200 or status_code > 299:
                # Only log response data if it's a non-200 response:
                response_content = response.content
                try:
                    response_data = json.loads(response_content)
                except ValueError:
                    if isinstance(response_content, dict):
                        response_data = response_content
                    else:
                        response_data_value = (
                            "<<<Stripped>>>" if response_content else None)
                        response_data = {'detail': response_data_value}
            
                response_context = {'data': response_data,
                                    'status_code': status_code}
            else:
                # A success HTTP response doesn't require logging data
                response_context = {'status_code': status_code}
                
        # Get the runtime
        try:
            run_time = time.time() - request.start_time
        except AttributeError:
            run_time = None
                     
        logger = logging.getLogger(settings.LOGGER_NAME_LOGGLY)
        logger.info(json.dumps({
                    'source': 'middleware.LoggingMiddleware',
                    'request': request_context,
                    'response': response_context,
                    'run_time': run_time
                    }))
        
        return response
        

class WebfactionFixes(object):
    """
    On WebFaction each host has its own Apache instance, with WebFaction's main
    Nginx instance forwarding request. So when a Django application's Apache 
    instance proxies requests to Django, the `REMOTE_ADDR` header is not set 
    with  the client's IP address. Instead, the IP address is available as the
    first IP address in the comma separated list in the `HTTP_X_FORWARDED_FOR`
    header.
    
    This middleware fixes this by automatically setting `REMOTE_ADDR` to the
    value of `HTTP_X_FORWARDED_FOR`
    
    Note:
        This middleware should be installed at the top of your list to restore
        the lost info.
        
    Ref: 
        https://docs.webfaction.com/software/django/troubleshooting.html#accessing-remote-addr
    """
    def __init__(self, get_response):
        self.get_response = get_response
        
    def __call__(self, request):
        # Code to be executed for each request before the view
        # (and later middleware) are called
        self.process_request(request)
        
        response = self.get_response(request)
        
        # Code to be executed for each request/response after the view is called
        # N/A
        
        return response
    
    def process_request(self, request):
        """
        Set's `REMOTE_ADDR` based on `HTTP_X_FORWARDED_FOR`, if the latter is 
        set.
        
        Args:
            request: HttpRequest object
            
        Returns:
            None, which allows Django to continue processing this request and
            execute other process_request() middleware.
        """
        if 'HTTP_X_FORWARDED_FOR' in request.META:
            ip = request.META['HTTP_X_FORWARDED_FOR'].split(",")[0].strip()
            request.META['REMOTE_ADDR'] = ip
