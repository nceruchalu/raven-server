{% extends "email/base.txt" %}
{% block content %}
Welcome to Raven!

We're happy you're here.

You recently registered {{ email }}, but it isn't activated yet. It's time 
to do that now.

To activate your account, follow the link below within the next 
{{ expiration_days }} days:
{{ activation_link }}

Your sign in email is:
{{ email }}
{% endblock %}
