"""
A set of request processors that return dictionaries to be merged into a
template context. Each function takes one argument, an HttpRequest object,
and returns a dictionary to add to the context.

These are referenced from the 'context_processors' option of the TEMPLATES 
setting.
"""

def email(request):
    """
    Adds (HTML) email-related context variables to the context.
    """
    # CSS variables
    color_black = "#373737"
    color_blue = "#0e7dd8"
    color_blue_dark = "#2162a5"
    color_blue_light = "#439fe0"
    color_gray = "#777777"
    content_padding = 20 # in pixels
    content_width = 400  # pixels
    font_size = "15px"
    footer_font_size = "12px"
    footer_width = 600 # pixels
    line_height = "22px"
    
    # CSS styles
    font_family = (
        "font-family: "
        "'Helvetica Neue', Helvetica, 'Roboto', Arial, sans-serif; ")
    font = ("{font_family} "
            "font-size: 15px; "
            "font-weight: normal; ").format(font_family=font_family)
    
    # outlook.com has decided to not support margins. However this can be fixed
    # by capitalizing the first character of each margin property
    # so Margin works where margin won't,
    # Ref: https://www.emailonacid.com/blog/article/email-development/outlook.com-does-support-margins
    return {
        # CSS styles
        'css_font_family': font_family,
        'css_font': font,
        
        'css_element': ("box-sizing: border-box; "
                        "-webkit-box-sizing: border-box; "
                        "-moz-box-sizing: border-box; "
                        "color: {color_black}; "
                        "{font_family} "
                        "font-weight: normal; "
                        "Margin-bottom: 0; "
                        "Margin-left: 0; "
                        "Margin-right: 0; "
                        "Margin-top: 0; "
                        "padding-bottom: 0; "
                        "padding-left: 0; "
                        "padding-right: 0; "
                        "padding-top: 0; ").format(color_black=color_black,
                                                   font_family=font_family),
        'css_table': ("Margin: 0; "
                      "padding: 0; "
                      "border-collapse: collapse; "
                      "mso-table-lspace: 0pt; "
                      "mso-table-rspace: 0pt; "),
        
        'css_td': ("Margin: 0; "
                   "padding: 0; "
                   "border-collapse: collapse; "),
        
        'css_td_padding_horizontal': ("Margin: 0; "
                                      "padding: 0; "
                                      "border-collapse: collapse; "
                                      "width: {width}; ").format(
            width=content_padding),
        
        'css_img': ("border: 0; "
                    "Margin: 0; "
                    "padding: 0 "
                    "vertical-align: middle; "),
        
        'css_p': ("{font} "
                  "line-height: 24px; "
                  "Margin: 0 0 16px; ").format(font=font),
        
        'css_p_footer': ("color: {color}; "
                         "{font_family} "
                         "font-size: {font_size}; "
                         "font-weight: normal; "
                         "line-height: 20px; "
                         "Margin: 0; "
                         "text-align: center; ").format(
            color=color_gray,
            font_family=font_family,
            font_size=footer_font_size),
        
        'css_p_small': ("color: {color}; "
                        "{font_family} "
                        "font-size: 14px; "
                        "font-weight: normal; "
                        "line-height: 18px; "
                        "Margin: 0 0 14px; ").format(
            color=color_black,
            font_family=font_family),
        
        'css_h2': ("font-size: 18px; "
                   "{font_family} "
                   "font-weight: bold; "
                   "line-height: 28px; "
                   "Margin: 0 0 18px; ").format(font_family=font_family),
        
        'css_a': ("color: {color}; "
                  "{font_family} "
                  "text-decoration: none; ").format(
            color=color_blue_light,
            font_family=font_family),
        
        'css_a_footer': ("color: {color}; "
                         "display: inline-block; "
                         "{font_family} "
                         "font-size: {font_size}; "
                         "padding-bottom: 4px; "
                         "text-decoration: underline; ").format(
            color=color_gray,
            font_family=font_family,
            font_size=footer_font_size),
                        
        'css_a_button': ("background-color: {background_color}; "
                         "border-radius: 2px; "
                         "color: white; "
                         "display: inline-block; "
                         "{font_family} "
                         "font-size: 14px; "
                         "font-weight: bold; "
                         "line-height: 16px; "
                         "min-height: 16px; "
                         "min-width: 54px; "
                         "padding: 10px 14px; "
                         "text-align: center;"
                         "text-decoration: none; ").format(
            background_color=color_blue,
            border_color=color_blue_dark,
            font_family=font_family),
        
        'css_p_button': ("color: {color}; "
                         "{font_family} "
                         "font-size: 12px; "
                         "font-weight: normal; "
                         "line-height: 18px; "
                         "Margin: 0; "
                         "text-align: center; ").format(
            color=color_gray,
            font_family=font_family),

        'css_li_small': ("color: {color}; "
                         "{font_family} "
                         "font-size: 14px; "
                         "font-weight: normal; "
                         "line-height: 18px; "
                         "Margin: 0; "
                         "padding: 0;").format(
            color=color_black,
            font_family=font_family),
        
        # CSS variables
        'css_var_color_black': color_black,
        'css_var_color_blue': color_blue,
        'css_var_color_gray': color_gray,
        'css_var_content_width': content_width,
        'css_var_font_size': font_size,
        'css_var_footer_width': footer_width,
        'css_var_line_height': line_height,
    }
