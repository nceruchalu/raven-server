"""raven URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic.base import RedirectView

from raven import views
from raven.views import TemplateView

# REST API URLS
api_urlpatterns = [
    url(r'^$', views.api_root, name='api_root'),
    url(r'', include('raven.apps.account.urls')),
    url(r'', include('raven.apps.uploads.urls')),
    # login and logout views for the browsable API
    url(r'^browse/',include('rest_framework.urls', namespace='rest_framework')),
]

urlpatterns = [
    # REST API URLS with version number
    url(r'^api/v1/', include(api_urlpatterns)),
    url(r'^api/',
        RedirectView.as_view(pattern_name="api_root", permanent=False)),
    
    # Home pages
    url(r'^$',
        RedirectView.as_view(pattern_name="api_root", permanent=False),
        name='home'),
    
    # Regular web URLs
    url(r'', include('raven.apps.account.web_urls')),
    url(r'', include('raven.apps.uploads.web_urls')),
    url(r'^404/$', TemplateView.as_view(template_name="404.html"), name='404'),
    url(r'^500/$', TemplateView.as_view(template_name="500.html"), name='500'),
    
    url(r'^admin/', admin.site.urls),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
        
        url(r'^test-email/activation/$', 
            TemplateView.as_view(
                template_name="account/activation_email.html",
                context_data={
                    'title': "Welcome to Raven",
                    'email': "hello@tryraven.com",
                    'activation_link': (
                        "https://tryraven.com/activate/"
                        "441f1181ecfc99a702ba35639099a40a25f48fbb/"),
                    'expiration_days': 4
            }),
            name='test-email-activation'),
        
        url(r'^test-email/password/reset/$', 
            TemplateView.as_view(
                template_name="account/password_reset_email.html",
                context_data={
                    'title': "Reset your password",
                    'reset_link': (
                        "https://tryraven.com/password/reset/"
                        "MQ/46m-44286d079b127d7c185/")
                    }),
            name='test-email-password-reset'),

        url(r'^test-email/upload/$', 
            TemplateView.as_view(
                template_name="uploads/forward_email.html",
                context_data={
                    'title': "You have been sent a file via Raven",
                    'email': "hello@tryraven.com",
                    'download_link': "https://tryraven.com/p/hello/",
                    "total_size": "123KB",
                    "files": ['hello.txt', 'world.py']
            }),
            name='test-email-forward'),
    ]


# Configure admin site
admin.site.site_header = "Raven administration"
